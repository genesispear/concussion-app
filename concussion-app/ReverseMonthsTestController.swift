//
//  ReverseMonthsTest.swift
//  concussion-app
//
//  Created by Nathaniel Ho on 5/21/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit

class ReverseMonthsController: OpenEarsViewController {
    @IBOutlet var nextButton: UIButton!
    var delButton: UIButton!
    var q2: UILabel!
    var a0: UILabel!
    var a1: UILabel!
    var a2: UILabel!
    var a3: UILabel!
    var a4: UILabel!
    var a0pos: CGPoint!
    var a1pos: CGPoint!
    var a2pos: CGPoint!
    var a3pos: CGPoint!
    var a4pos: CGPoint!

    var score: Int = 1
    
    var months: [String] = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE",
        "JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"]
    var monthsSaid = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set Up the Nav Bar
        self.navigationItem.title = "Memory Assessment"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        nextButton.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        nextButton.enabled = false;
        
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        //insert Header "Date and Time"
        var header = UILabel(frame: CGRectMake(0, 0, screenWidth, 60))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 40)
        header.layer.cornerRadius = 0
        header.layer.masksToBounds = true
        header.text = "Month Reversal"
        //header.backgroundColor = UIColor(hexString:"#e4cdee")
        
        self.view.addSubview(header)
        
        //insert first question
        var q1 = UILabel(frame: CGRectMake(0, 0, screenWidth, 50))
        q1.center = CGPointMake((screenWidth/2), (screenHeight/4))
        q1.textAlignment = NSTextAlignment.Center
        
        q1.font = UIFont(name: q1.font.fontName, size: 20)
        q1.layer.cornerRadius = 0
        q1.layer.masksToBounds = true
        q1.text = "State months in reverse order"
        //q1.backgroundColor = UIColor(hexString:"#e4cdee")
        
        self.view.addSubview(q1)
        
        q2 = UILabel(frame: CGRectMake(0, 0, screenWidth, 50))
        q2.center = CGPointMake((screenWidth/2), (screenHeight/3))
        q2.textAlignment = NSTextAlignment.Center
        
        q2.font = UIFont(name: q2.font.fontName, size: 20)
        q2.layer.cornerRadius = 0
        q2.layer.masksToBounds = true
        q2.text = "You said..."
        q2.alpha = 0.0
        
        self.view.addSubview(q2)
        
        a0 = UILabel(frame: CGRectMake(0, 0, screenWidth, 50))
        a0pos = CGPointMake((screenWidth/2), (screenHeight*(5/12)))
        a0.center = a0pos
        a0.textAlignment = NSTextAlignment.Center
        
        a0.font = UIFont(name: a0.font.fontName, size: 30)
        a0.layer.cornerRadius = 0
        a0.layer.masksToBounds = true
        a0.text = ""
        a0.alpha = 0.0
        
        self.view.addSubview(a0)
        
        a1 = UILabel(frame: CGRectMake(0, 0, screenWidth, 50))
        a1pos = CGPointMake((screenWidth/2), (screenHeight*(5/12)))
        a1.center = a1pos
        a1.textAlignment = NSTextAlignment.Center
        
        a1.font = UIFont(name: a1.font.fontName, size: 30)
        a1.layer.cornerRadius = 0
        a1.layer.masksToBounds = true
        a1.text = ""
        a1.alpha = 0.0
        
        self.view.addSubview(a1)
        
        a2 = UILabel(frame: CGRectMake(0, 0, screenWidth, 50))
        a2pos = CGPointMake((screenWidth/2), (screenHeight*(7/12)))
        a2.center = a2pos
        a2.textAlignment = NSTextAlignment.Center

        a2.font = UIFont(name: a2.font.fontName, size: 30)
        a2.layer.cornerRadius = 0
        a2.layer.masksToBounds = true
        a2.text = ""
        a2.alpha = 0.66

        self.view.addSubview(a2)
        
        a3 = UILabel(frame: CGRectMake(0, 0, screenWidth, 50))
        a3pos = CGPointMake((screenWidth/2), (screenHeight*(3/4)))
        a4pos = CGPointMake((screenWidth/2), (screenHeight*(5/6)))
        a3.center = a3pos
        a3.textAlignment = NSTextAlignment.Center

        a3.font = UIFont(name: a3.font.fontName, size: 30)
        a3.layer.cornerRadius = 0
        a3.layer.masksToBounds = true
        a3.text = ""
        a3.alpha = 0.33

        self.view.addSubview(a3)
        
        a4 = UILabel(frame: CGRectMake(0, 0, screenWidth, 50))
        a4pos = CGPointMake((screenWidth/2), (screenHeight*(5/6)))
        a4.center = a4pos
        a4.textAlignment = NSTextAlignment.Center
        
        a4.font = UIFont(name: a4.font.fontName, size: 30)
        a4.layer.cornerRadius = 0
        a4.layer.masksToBounds = true
        a4.text = ""
        a4.alpha = 0.0
        
        self.view.addSubview(a4)

        var ypos = 0.95 * screenHeight
        var buttonh = screenHeight - 0.90 * screenHeight - 5
        delButton = UIButton(frame: CGRectMake(0,0,screenWidth, buttonh))
        delButton.setTitle("Delete", forState: .Normal)
        delButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        delButton.center = CGPointMake(screenWidth/2, ypos)
        delButton.layer.borderWidth = 1
        delButton.layer.cornerRadius = 5;
        delButton.backgroundColor = UIColor(hexString: buttonColor)
        delButton.alpha = 0.0
        delButton.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        
        self.view.addSubview(delButton)
        
        // Text-To-Speech command for test
        voiceCommand("Now tell me the months of the year in reverse order -- start with the last month and go backward -- For example -- you could say -- December -- November  -- now go ahead.")
        
        //start voice recognition
        addWords()
        changeLanguageModel()
    }
    
    // event is fired AT THE END OF THE VOICE DICTATION
    override func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didFinishSpeechUtterance utterance: AVSpeechUtterance!) {
        resumeRecognition()
    }
    
    func buttonAction(sender: UIButton) {
        if(sender == delButton && monthsSaid.count > 0) {
            monthsSaid.removeLast()
            // deletion animation (labels move up)
            UIView.animateWithDuration(0.5, animations: {
                self.a1.alpha = 0.0
                self.a2.center = self.a1pos
                self.a2.alpha = 1.0
                self.a3.center = self.a2pos
                self.a3.alpha = 0.66
                self.a4.center = self.a3pos
                self.a4.alpha = 0.33
                }, completion: {
                    (finished: Bool) in
                    self.a1.text = self.a2.text
                    self.a1.alpha = 1.0
                    self.a1.center = self.a1pos
                    self.a2.text = self.a3.text
                    self.a2.alpha = 0.66
                    self.a2.center = self.a2pos
                    self.a3.text = self.a4.text
                    self.a3.alpha = 0.33
                    self.a3.center = self.a3pos
                    if(self.monthsSaid.count - 4 >= 0) {
                        self.a4.text = self.monthsSaid[self.monthsSaid.count - 4]
                    }
                    else {
                        self.a4.text = ""
                    }
                    self.a4.alpha = 0.0
                    self.a4.center = self.a4pos
                    return
            })
        }
        if(sender == nextButton) {
            if(checkAnswer()) {
                score = 1
            }
            else {
                score = 0
            }
            println("\(score)")
            GlobalVariables.sharedInstance.concentrationScore += score
            performSegueWithIdentifier("MonthReversalTestSegue", sender: self)
        }
    }
    
    func checkAnswer() -> Bool {
        var index: Int
        // if number of months said is not 12, 
        if(monthsSaid.count < 12 || monthsSaid.count > 12) {
            return false
        }
        // monthsSaid.count == 12
        for index = 0; index < 12; index++ {
            if(months[11 - index] != monthsSaid[index]) {
                break
            }
        }
        if(index < 12) {
            return false
        }
        return true
    }
    
    /**
    *  BEGIN VOICE RECOGNITION METHODS
    */
    
    // on exit of scene stop listening
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        suspendRecognition()
        removeWords()
        removeButtons()
        self.openEarsEventsObserver.delegate = nil
        self.openEarsEventsObserver = nil
        
    }
    
    // method adds months for voice recognition
    override func addWords() {
        words.append("JANUARY")
        words.append("FEBRUARY")
        words.append("MARCH")
        words.append("APRIL")
        words.append("MAY")
        words.append("JUNE")
        words.append("JULY")
        words.append("AUGUST")
        words.append("SEPTEMBER")
        words.append("OCTOBER")
        words.append("NOVEMBER")
        words.append("DECEMBER")
        words.append("NEXT")
    }
    
    // method receives data String from voice recognition, indexes into dictionary to receive reference to UIButton
    override func pocketsphinxDidReceiveHypothesis(hypothesis: String, recognitionScore: String, utteranceID: String) {
        var word = hypothesis.componentsSeparatedByString(" ")[0]
        if(word == "NEXT") {
            if(nextButton.enabled == false) {
                return
            }
            if(checkAnswer()) {
                score = 1
            }
            else {
                score = 0
            }
            println("\(score)")
            GlobalVariables.sharedInstance.concentrationScore += score
            performSegueWithIdentifier("MonthReversalTestSegue", sender: self)
        }
        else {
            nextButton.enabled = true
            if(self.q2.alpha == 0.0) {
                a1.text = word
                UIView.animateWithDuration(0.5, animations: {
                    self.q2.alpha = 1.0
                    self.a1.alpha = 1.0
                    self.delButton.alpha = 1.0
                })
            }
            else {
                a0.text = word
                UIView.animateWithDuration(0.5, animations: {
                    self.a0.alpha = 1.0
                    self.a1.center = self.a2pos
                    self.a1.alpha = 0.66
                    self.a2.center = self.a3pos
                    self.a2.alpha = 0.33
                    self.a3.center = self.a4pos
                    self.a3.alpha = 0.0
                    }, completion: {
                        (finished: Bool) in
                        self.a4.text = self.a3.text
                        self.a3.text = self.a2.text
                        self.a3.alpha = 0.33
                        self.a3.center = self.a3pos
                        self.a2.text = self.a1.text
                        self.a2.alpha = 0.66
                        self.a2.center = self.a2pos
                        self.a1.text = word
                        self.a1.alpha = 1.0
                        self.a1.center = self.a1pos
                        self.a0.text = ""
                        self.a0.alpha = 0.0
                        return
                })
            }
            println("hypothesis: \(hypothesis)")
            monthsSaid.append(word)
        }
    }

    /**
    *  END VOICE RECOGNITION METHODS
    */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}