//
//  wordMemoryScene_test.swift
//  concussion-app
//
//  Created by Alex Rumbaugh on 2/21/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

//
//  ViewController.swift
//  concussion-app
//
//  Created by Nathaniel Ho on 2/16/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit

class wordMemoryTestViewController: OpenEarsViewController {

    
    @IBOutlet var nextButton : UIButton!
    @IBOutlet var selectedLabel : UILabel!
    @IBOutlet var instructionsLabel : UILabel!
    @IBOutlet var delButton: UIButton!
    @IBOutlet var dontknowButton: UIButton!
    
    @IBOutlet var wordLabels: [UILabel] = []
    
    var selectedWords = [String]()
    
    var currLabel = 0
    
    @IBAction func chooseNextScene(sender: UIButton) {
        if (GlobalVariables.sharedInstance.delayedRecallFlag == 0)
        {
            wordMemoryTestInst.currentIteration++
            updateScore()
            if (wordMemoryTestInst.currentIteration < 3)
            {
                performSegueWithIdentifier("nextIteration", sender: sender)
            }
            else
            {
                GlobalVariables.sharedInstance.delayedRecallFlag = 1
                performSegueWithIdentifier("nextScene", sender: sender)
            }
        }
        else
        {
            updateScore()
            performSegueWithIdentifier("scoreScreen", sender: sender)
        }
    }
    
    @IBAction func delButtonPress(sender: UIButton) {
        if(currLabel > 0)
            {
                    nextButton.enabled = false
                    selectedWords.removeLast()
                    wordLabels[currLabel-1].text = ""
                    currLabel--
            }
        
    }
    
    @IBAction func dontknowButtonPress(sender: UIButton) {
        if(currLabel < 5)
        {
                selectedWords.append("I Don't Know")
                wordLabels[currLabel].text = "I Don't Know"
                currLabel++
            if(currLabel == 5)
            {
                nextButton.enabled = true
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup navigation bar
        self.navigationItem.title = "Cognitive Assessment"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        //nextButton.enabled = false
        
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        //padding
        let ypad_top : CGFloat = screenHeight * 0.30
        let ypad_bot : CGFloat = screenHeight * 0.35
        let xpad_left : CGFloat = screenWidth * 0.05
        let xpad_right : CGFloat = screenWidth * 0.05
        
        var ycell = (screenHeight - 1.9*ypad_top) / 4.0
        
        //width & height for the buttons
        var buttonw :CGFloat = 108
        var buttonh :CGFloat = 32
        
        var xpos: CGFloat = 0
        var ypos: CGFloat = 0

        
        var header = UILabel(frame: CGRectMake(0, 0, screenWidth, 45))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 36)
        header.layer.cornerRadius = 5
        header.layer.masksToBounds = true
        header.text = "Immediate Memory"
        
        self.view.addSubview(header)
        
        //insert first question
        var q2 = UILabel(frame: CGRectMake(0, 0, 280, 45))
        q2.center = CGPointMake((screenWidth/2), (screenHeight/4))
        q2.textAlignment = NSTextAlignment.Center
        q2.font = UIFont(name: q2.font.fontName, size: 20)
        q2.layer.cornerRadius = 5
        q2.layer.masksToBounds = true
        q2.text = "What words were shown?"
        
        self.view.addSubview(q2)
        
        var i = 0
        var row : CGFloat = 0
        for label in wordLabels
        {
            label.text = ""
            // Set up UI elements for the buttons
            label.layer.borderWidth = 1;
            label.layer.cornerRadius = 3;
            label.backgroundColor = UIColor(hexString: buttonColor)
            label.frame = CGRectMake(0,0, buttonw, buttonh)
            var loc = CGPointMake(screenWidth / 2.0, ypad_top + row*ycell + ycell / 2.0)
            label.center = loc
            i++
            row++
        }
        i = 0
        
        //Place I Don't Know and Delete Buttons
        xpos = screenWidth / 4.0
        ypos = 0.95 * screenHeight
        buttonh = screenHeight - 0.90 * screenHeight - 5
        dontknowButton.frame = CGRectMake(0,0,screenWidth / 2.0 - 3,  buttonh)
        dontknowButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        dontknowButton.center = CGPointMake(xpos, ypos)
        dontknowButton.layer.borderWidth = 1
        dontknowButton.layer.cornerRadius = 5;
        dontknowButton.backgroundColor = UIColor(hexString: buttonColor)
        delButton.frame = CGRectMake(0,0,screenWidth / 2.0 - 3, buttonh)
        delButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        delButton.center = CGPointMake(xpos * 3, ypos)
        delButton.layer.borderWidth = 1
        delButton.layer.cornerRadius = 5;
        delButton.backgroundColor = UIColor(hexString: buttonColor)
        
        nextButton.enabled = false
        // Text-To-Speech command for test
        if ( GlobalVariables.sharedInstance.delayedRecallFlag == 0)
        {
            voiceCommand("Repeat back as many words as you can remember in any order.")
        }
        else
        {
            voiceCommand("Do you remember that list of words I read a few times earlier. Tell me as many words from the list as you can remember in any order")
        }
        
        // Voice Recognition
        addWords()
        changeLanguageModel()
    }
    
    // event is fired AT THE END OF THE VOICE DICTATION
    override func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didFinishSpeechUtterance utterance: AVSpeechUtterance!) {
        resumeRecognition()
    }
    
    // change the color of the button once it is selected
    func toggleColor(sender: UIButton ){
        if(sender.backgroundColor == UIColor.whiteColor())
        {
            sender.layer.backgroundColor = sender.layer.borderColor
            sender.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        }
        else
        {
            sender.backgroundColor = UIColor.whiteColor()
            sender.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        }
    }
    
    // update the score for the test
    func updateScore(){
        var score = 0
        for word in selectedWords
        {
            let mutatedString = word.capitalizedString
            if(contains(wordMemoryTestInst.chosenWords,mutatedString))
            {
                score++
            }
        }
        if(GlobalVariables.sharedInstance.delayedRecallFlag == 0)
        {
            GlobalVariables.sharedInstance.wordMemoryTestScore[wordMemoryTestInst.currentIteration-1] = score
            GlobalVariables.sharedInstance.immediateMemoryScore += score
        }
        else
        {
            GlobalVariables.sharedInstance.delayedRecallScore += score
        }
    }

    // on exit of scene stop listening
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        suspendRecognition()
        removeWords()
        removeButtons()
        self.openEarsEventsObserver.delegate = nil
        self.openEarsEventsObserver = nil
    }
    
    

    
    /**
    *  BEGIN VOICE RECOGNITION METHODS
    */
    
    override func addWords() {
        for word in wordMemoryTestInst.availableWords
        {
            words.append(word.uppercaseString)
        }
        words.append("NEXT")
    }
    
    override func pocketsphinxDidReceiveHypothesis(hypothesis: String, recognitionScore: String, utteranceID: String) {
        print("Word Test: ")
        let wordtext = hypothesis.componentsSeparatedByString(" ")[0] as NSString
        println(hypothesis)
        if(currLabel < 5 && hypothesis != "NEXT" && !contains(selectedWords,wordtext as String))
        {
            selectedWords.append(wordtext as String)
            wordLabels[currLabel].text = wordtext as String
            currLabel++
            if(currLabel == 5) {
                nextButton.enabled = true
            }
        }
        if(currLabel == 5 && hypothesis == "NEXT") {
            wordMemoryTestInst.currentIteration++
            if (wordMemoryTestInst.currentIteration < 3)
            {
                performSegueWithIdentifier("nextIteration", sender: self)
            }
            else
            {
                performSegueWithIdentifier("nextScene", sender: self)
            }
        }
    }

    /**
    *  END VOICE RECOGNITION METHODS
    */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

