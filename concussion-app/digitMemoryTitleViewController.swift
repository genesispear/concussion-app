//
//  digitMemory_title.swift
//  concussion-app
//
//  Created by Alex Rumbaugh on 3/1/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit

class digitMemoryTitleViewController: OpenEarsViewController {
    
    
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var instructionsLabel: UILabel!
    
    @IBOutlet var digits: [UILabel] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup navigation bar title
        self.navigationItem.title = "Cognitive Assessment"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        //border dimensions and measurements
        var bounds: CGRect = UIScreen.mainScreen().bounds
        var width:CGFloat = bounds.size.width
        var height:CGFloat = bounds.size.height
        let ypad_top : CGFloat = height * 0.15
        let ypad_bot : CGFloat = height * 0.15
        let xpad_left : CGFloat = width * 0.05
        let xpad_right : CGFloat = width * 0.05
        
        
        
        //width & height of the buttons
        let buttonw :CGFloat = 64
        let buttonh :CGFloat = 64
        
        //used to position the digits on the screen
        var numDigits:CGFloat = CGFloat(digitMemoryTestInst.digitsPerTrial[digitMemoryTestInst.currentTrial])
        let cellx : CGFloat = (width - xpad_left - xpad_right)/numDigits
        let celly : CGFloat = (height - ypad_top - ypad_bot)
        
        //width & height of the title text
        var instLabw : CGFloat = 512
        var instLabh : CGFloat = 64
        
        //font size variables
        var digitFontSize : CGFloat = 48
        var titleFontSize : CGFloat = 20
        
        var header = UILabel(frame: CGRectMake(0, 0, 280, 45))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 36)
        header.layer.cornerRadius = 5
        header.layer.masksToBounds = true
        header.text = "Concentration"
        self.view.addSubview(header)
        
        //insert first question
        var q2 = UILabel(frame: CGRectMake(0, 0, 280, 45))
        q2.center = CGPointMake((screenWidth/2), (screenHeight/4))
        q2.textAlignment = NSTextAlignment.Center
        q2.font = UIFont(name: q2.font.fontName, size: 20)
        q2.layer.cornerRadius = 5
        q2.layer.masksToBounds = true
        q2.text = "Remember the following digits"
        self.view.addSubview(q2)
        
        
        //generate the digits for this trial
        digitMemoryTestInst.generateDigits()
        
        //set the labels with digits
        var i = 0
        for digitLabel in digits
        {
            digitLabel.backgroundColor = UIColor.whiteColor()
            if ( i >= Int(numDigits))
            {
                /// don't display it!
            }
            else
            {
                digitLabel.text = digitMemoryTestInst.chosenDigits[i]
                var xpos: CGFloat = xpad_left + cellx*CGFloat(i) + cellx/2.0
                var ypos: CGFloat = ypad_top + celly/2.0
                digitLabel.frame = CGRectMake(xpos,ypos,buttonw, buttonh)
                digitLabel.center = CGPoint(x: xpos,y: ypos)
                digitLabel.backgroundColor = UIColor(hexString: bgColor)
                digitLabel.font = UIFont(name: digitLabel.font.fontName, size: digitFontSize)
            }
            i++
        }
        
        //nextButton.enabled = false
        nextButton.hidden = true
        // Text-To-Speech command for test
        
        
        // construct string for voice command
        var wordSpeech = "-- "
        for digit in digitMemoryTestInst.chosenDigits
        {
            wordSpeech += digit
            wordSpeech += ". -- "
        }
        
        if (digitMemoryTestInst.currentTrial == 0)
        {
            voiceCommand("I am go ing to read you a string of numbers -- when I am done  -- repeat them back to me in reverse order  --- of how I read them to you  -- if I say -- 7 -- 1 -- 9 -- you would say --  9 -- 1 -- 7 -- Given the instructions -- the numbers to remember are -- " + wordSpeech)
        }
        else
        {
            voiceCommand("I am go ing to repeat a number list again -- Repeat back -- as many numbers as you can remember -- in reverse order -- The numbers to remember are -- " + wordSpeech)
        }
        
        //start voice recognition
        addWords()
        changeLanguageModel()
    }
    
    // event is fired AT THE END OF THE VOICE DICTATION
    override func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didFinishSpeechUtterance utterance: AVSpeechUtterance!) {
        resumeRecognition()
        var timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("nextScene"), userInfo: nil, repeats: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func nextScene() {
        performSegueWithIdentifier("DigitMemorySegue", sender: self)
    }
    
    /**
    *  BEGIN VOICE RECOGNITION METHODS
    */
    
    // on exit of scene stop listening
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        suspendRecognition()
        removeWords()
        removeButtons()
        self.openEarsEventsObserver.delegate = nil
        self.openEarsEventsObserver = nil
        
    }
    
    // method adds months for voice recognition
    override func addWords() {
        words.append("NEXT")
    }
    
    // method receives data String from voice recognition, indexes into dictionary to receive reference to UIButton
    override func pocketsphinxDidReceiveHypothesis(hypothesis: String, recognitionScore: String, utteranceID: String) {
        
        if(hypothesis == "NEXT") {
            performSegueWithIdentifier("DigitMemorySegue", sender: self)
        }
    }
}
