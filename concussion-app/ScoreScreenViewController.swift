//
//  ScoreScreenViewController.swift
//  concussion-app
//
//  Created by Michael Mathew on 5/19/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import Foundation




class ScoreScreenViewController: OpenEarsViewController {
    
    var scoreScreenItems: [String] = ["Num of Symptoms of 22","Symptom Severity of 132","Orientation of 5","Immediate Memory of 15","Concentration of 5","Delayed Recall of 5", "SAC Total", "BESS (total errors)"]
    var scoreScreenValues: [Int] = [GlobalVariables.sharedInstance.symTotal, GlobalVariables.sharedInstance.symSeverityScore, GlobalVariables.sharedInstance.orientationScore,GlobalVariables.sharedInstance.immediateMemoryScore,GlobalVariables.sharedInstance.concentrationScore,GlobalVariables.sharedInstance.delayedRecallScore, GlobalVariables.sharedInstance.getSacTotalScore(), GlobalVariables.sharedInstance.balanceTestScore]
    
    override func viewDidLoad() {
        
        stopListening()
        //setup navigation bar
        self.navigationItem.title = "Assessment Results"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        //padding
        let ypad_top : CGFloat = screenHeight * 0.30
        let ypad_bot : CGFloat = screenHeight * 0.35
        let xpad_left : CGFloat = screenWidth * 0.05
        let xpad_right : CGFloat = screenWidth * 0.05
        
        var ycell = (screenHeight - 1.9*ypad_top) / 7.0
        
        //width & height for the buttons
        var buttonw :CGFloat = 108
        var buttonh :CGFloat = 32
        
        var xpos: CGFloat = 0
        var ypos: CGFloat = 0
        
        // position on screen where we divided the view
        let dividingLine = (screenWidth / 3) * 2
        
        // position on screen for columns "test domain" and "score"
        let testDomain = (dividingLine / 2) - 35
        let score = screenWidth - testDomain + 50
        
        
        // set up the header
        var header = UILabel(frame: CGRectMake(0, 0, 280, 45))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 36)
        header.layer.cornerRadius = 5
        header.layer.masksToBounds = true
        header.text = "Scoring Summary"
        header.sizeToFit()
        
        self.view.addSubview(header)
        
        
        // set up the sub header
        var col1Header = UILabel(frame: CGRectMake(0, 0, 100, 30))
        col1Header.center = CGPointMake(testDomain, (screenHeight/4))
        col1Header.textAlignment = NSTextAlignment.Center
        col1Header.font = UIFont(name: col1Header.font.fontName, size: 20)
        col1Header.layer.cornerRadius = 5
        col1Header.layer.masksToBounds = true
        col1Header.text = "Test Domain"
        col1Header.sizeToFit()
        
        self.view.addSubview(col1Header)
        
        // set up the column headers
        var col2Header = UILabel(frame: CGRectMake(0, 0, 100, 30))
        col2Header.center = CGPointMake(score, (screenHeight/4))
        col2Header.textAlignment = NSTextAlignment.Center
        col2Header.font = UIFont(name: col2Header.font.fontName, size: 20)
        col2Header.layer.cornerRadius = 5
        col2Header.layer.masksToBounds = true
        col2Header.text = "Score"
        col2Header.sizeToFit()
        
        self.view.addSubview(col2Header)
        
        println(String(GlobalVariables.sharedInstance.delayedRecallScore))
        
        
        // set up the UILabel layout for all the score sections
        var row : CGFloat = 0
        for (var i = 0; i < scoreScreenItems.count; i++)
        {
            
            if (i == 5) {
                println(String(scoreScreenItems[i]))
                println(String(scoreScreenValues[i]))
            }
            let yValue = ypad_top + row*ycell + ycell / 2.0
            
            var scoreStringLabel = UILabel(frame: CGRectMake(0, 0, 100, 30))
            scoreStringLabel.text = scoreScreenItems[i]
            var loc = CGPointMake(testDomain, yValue)
            if (scoreScreenItems[i] == "SAC Total") {
                loc = CGPointMake(testDomain + 110, yValue)
            }
            scoreStringLabel.center = loc
            scoreStringLabel.sizeToFit()
            self.view.addSubview(scoreStringLabel)
            
            var scoreLabel = UILabel(frame: CGRectMake(0, 0, 100, 30))
            scoreLabel.text = String(scoreScreenValues[i])
            var loc2 = CGPointMake(score + 23, yValue)
            scoreLabel.center = loc2
            scoreLabel.sizeToFit()
            self.view.addSubview(scoreLabel)
            
            row++
        }
        
        // return home for score screen
        var returnHome = UIButton(frame: CGRectMake(0,screenHeight * 0.85,screenWidth,screenHeight*0.15))
        returnHome.setTitle("Return Home", forState: UIControlState.Normal)
        returnHome.backgroundColor = UIColor(hexString: "#a85ac9")
        returnHome.addTarget(self, action: "returnHome", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(returnHome)
    }
    
    func returnHome() {
        performSegueWithIdentifier("score2Menu", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        GlobalVariables.sharedInstance.resetAllScoreVars()
    }
}