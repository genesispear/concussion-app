//
//  NV_BalanceTestViewController.swift
//  concussion-app
//
//  Created by Michael Mathew on 5/28/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//
import Foundation
import UIKit

/*
switch testIndex {
case 0:
voiceCommand("This is the balance test --- the first stance is the double leg stance. -- standing with your feet together -- and with your eyes closed -- You should hold the phone in front of you --  You should try to maintain stability in that position for 20 seconds -- I will be counting the number of times you move out of this position -- Please push the start button -- or  say start -- when you are ready")
case 1:
voiceCommand("Next is the Single Leg Stance -- If you were to kick a ball -- This will be with your dominant foot -- Stand on your non-dominant foot. --- The dominant leg should be held in approximately thirty degrees of hip flexion --- and 45 degrees of knee flexion. -- Again, you should try to maintain stability for twenty seconds with the phone in front of you --- and your eyes closed. --- I will be counting the number of times you move out of this position. ---  If you stumble out of this position --- open your eyes --- return to the start position and continue balancing. --- I will start timing when you are set - Please push the start button -- or  say start -- when you are ready")
case 2:
voiceCommand("Next is the Tandem Stance -- Please stand heel to toe -- with your non-dominant foot in back --- Your weight should be evenly distributed - across both feet --- Again, you should try to maintain stability for twenty seconds with the phone in front of you --- and your eyes closed. --- I will be counting the number of times you move out of this position. ---  If you stumble out of this position --- open your eyes --- return to the start position and continue balancing. --- I will start timing when you are set - please say start -- or push the start button -- when you are ready")
default:
*/

class NV_BalanceTestViewController: UIViewController {
    
    var timer = NSTimer()
    
    @IBOutlet var testTitle: UILabel!
    @IBOutlet var numOfErrors: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var numOfErrorsHeader: UILabel!
    @IBOutlet var timeLabelHeader: UILabel!
    
    //UI buttons at bottom of screen for interaction
    var startTestButton: UIButton!
    var addErrorButton: UIButton!
    
    // variables for test
    var errors = 0
    var counter = 0
    var testDone = false
    
    // index specifies which balance test we are on
    let balanceTestDictionary:[Int:String] = [0:"Double Leg Stance", 1:"Single Leg Stance", 2:"Tandem Stance", 3: "Finished"];
    //var testIndex = 0
    
    // used for placement of UILabels
    var xloc = screenWidth / 2.0;
    var yloc = screenHeight / 7.0 + 5.0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup navigation bar title
        self.navigationItem.title = "Balance Test"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        // sets up the background color
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        // start button
        startTestButton = UIButton(frame: CGRectMake(0,screenHeight * 0.85,screenWidth,screenHeight * 0.15 ))
        startTestButton.setTitle("Start", forState: UIControlState.Normal)
        startTestButton.backgroundColor = UIColor(hexString:rightbright)
        startTestButton.addTarget(self, action: "startTestButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        startTestButton.hidden = false
        self.view.addSubview(startTestButton)
        
        // add 1 error button
        addErrorButton = UIButton(frame: CGRectMake(0,screenHeight * 0.85,screenWidth,screenHeight * 0.15 ))
        addErrorButton.setTitle("Add 1 Error", forState: UIControlState.Normal)
        addErrorButton.backgroundColor = UIColor(hexString:wrongbright)
        addErrorButton.addTarget(self, action: "errorButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        addErrorButton.hidden = true
        self.view.addSubview(addErrorButton)
        
        setUpLabels()
    }
    
    // this function controls what happens when user taps on screen UIButton to start
    func startTestButtonPressed() {
        startTestButton.hidden = true
        addErrorButton.hidden = false
        
        startBalanceTest()
    }
    
    // updates Number of Errors Label on view
    func errorButtonPressed() {
        if (errors < 10) {
            errors++
            numOfErrors?.text = "\(errors)"
            numOfErrors?.sizeToFit()
            numOfErrors?.center = CGPoint(x: xloc, y:yloc*3)
        }
        else {
            addErrorButton.backgroundColor = UIColor(hexString:wrongfade)
        }
    }
    
    // Used for resetting/ formatting labels to initial state
    func setUpLabels() {
        testTitle?.text = balanceTestDictionary[balanceTestIndex]
        numOfErrors?.text = "0"
        timeLabel?.text = "20"
        
        testTitle?.sizeToFit()
        numOfErrors?.sizeToFit()
        timeLabel?.sizeToFit()
        
        testTitle?.center = CGPoint(x: xloc, y:yloc)
        numOfErrorsHeader?.center = CGPoint(x: xloc, y:yloc*2)
        numOfErrors?.center = CGPoint(x: xloc, y:yloc*3)
        timeLabelHeader?.center = CGPoint(x: xloc, y:yloc*4)
        timeLabel?.center = CGPoint(x: xloc, y:yloc*5)
        startTestButton.hidden = false
    }
    
    // reset all scene state variables
    func resetSceneVariables() {
        errors = 0
        counter = 0
        testDone = false
    }
    
    // used to distinguish between which balance test we are on independent or part of scat
    func balanceTestTransition() {
        balanceTestIndex = 0
        testDone = true
        
        if (GlobalVariables.sharedInstance.testStateFlag == 1) {
            GlobalVariables.sharedInstance.delayedRecallFlag = 1
            performSegueWithIdentifier("Balance2DelayedRecall", sender: self)
        }
        else if (GlobalVariables.sharedInstance.testStateFlag == 2) {
            resultsOfBalanceTestView()
        }
        else {
            // some shit here
        }
    }
    
    // starts the balance test / intializes the NSTimer
    func startBalanceTest() {
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("updateCounter"), userInfo: nil, repeats: true)
    }
    
    // function is used as delegate to NSTimer to update countdown on screen and transition to next test
    func updateCounter() {
        // stop timer at 20 / current test is over
        if(counter == 20) {
            timer.invalidate()
            addErrorButton.hidden = true
            addErrorButton.backgroundColor = UIColor(hexString:wrongbright)
            
            // record each of the errors in our global variable for score screen
            GlobalVariables.sharedInstance.balanceTestScore += errors
            balanceTestIndex++;
            
            if (balanceTestIndex == 3) {
                balanceTestTransition()
            }
            else {
                resetSceneVariables()
                setUpLabels()
                performSegueWithIdentifier("NextTestSegue", sender: self)
            }
        }
        else {
            // update timer label on scene
            timeLabel?.text = "\(20 - counter++)"
            timeLabel?.sizeToFit()
            timeLabel?.center = CGPoint(x: xloc, y:yloc*5);
        }
    }
    
    // used for case when user completes independent balance test so they can see score
    func resultsOfBalanceTestView () {
        // set up the labels for the score screen
        testTitle?.text = "Balance Test Results"
        numOfErrorsHeader?.text = "Total number of Errors: (max 10)"
        numOfErrors?.text = String(GlobalVariables.sharedInstance.balanceTestScore)
        timeLabelHeader?.text = ""
        timeLabel?.text = ""
        
        testTitle?.sizeToFit()
        numOfErrorsHeader?.sizeToFit()
        numOfErrors?.sizeToFit()
        
        testTitle?.center = CGPoint(x: xloc, y:yloc)
        numOfErrorsHeader?.center = CGPoint(x: xloc, y:yloc*2)
        numOfErrors?.center = CGPoint(x: xloc, y:yloc*3)
        
        // enable return home button
        startTestButton.setTitle("Return Home", forState: UIControlState.Normal)
        startTestButton.layer.borderColor = UIColor.whiteColor().CGColor
        startTestButton.layer.backgroundColor = UIColor(hexString: "#a85ac9")?.CGColor
        startTestButton.hidden = false
    }
    
    // on scene change perform clean up actions
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        resetSceneVariables()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
