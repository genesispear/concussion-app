//
//  BalanceTestViewController.swift
//  concussion-app
//
//  Created by Michael Mathew on 5/9/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//
import UIKit
import CoreMotion

class BalanceTestViewController: OpenEarsViewController {
    
    @IBOutlet var testTitle: UILabel!
    @IBOutlet var numOfErrors: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var numOfErrorsHeader: UILabel!
    @IBOutlet var timeLabelHeader: UILabel!
    
    // this function controls what happens when user taps on screen UIButton
    @IBAction func startTestButtonPressed() {
        startTestButton.hidden = true
        // start the test via the scene button
        if (testDone != true) {
            startBalanceTest()
            suspendRecognition()
        }
            // test is done, return to home screen
        else {
            GlobalVariables.sharedInstance.resetAllScoreVars()
            performSegueWithIdentifier("returnToHomeScreenFromBalance", sender: self)
        }
    }
    
    // initialize objects for CoreMotion and NSTimer
    var motionManager = CMMotionManager()
    var timer = NSTimer()
    
    // variables for test
    var errors = 0
    var firstGyroIteration = false
    var initialStateGyroValue = 0.0
    var counter = 0
    var timeOfLastErr = 0
    var testDone = false
    
    var startTestButton: UIButton!
    
    // index specifies which balance test we are on
    let balanceTestDictionary:[Int:String] = [0:"Double Leg Stance",1:"Single Leg Stance",2:"Tandem Stance",3: "Finished"];
    var testIndex = 0
    
    // used for placement of UILabels
    var xloc = screenWidth / 2.0;
    var yloc = screenHeight / 7.0 + 5.0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*******FOR TESTING PURPOSES SKIP BALANCE TEST AND GO STRAIGHT TO SCORE SCREEN **********/
        //performSegueWithIdentifier("ScoreScreenSegue", sender: self)
        /******DELETE THIS AFTERWARDS*******/
        
        
        //setup navigation bar title
        self.navigationItem.title = "Balance Test"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        // sets up the background color
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        // start button
        startTestButton = UIButton(frame: CGRectMake(0,screenHeight * 0.85,screenWidth,screenHeight * 0.15 ))
        startTestButton.setTitle("Start", forState: UIControlState.Normal)
        startTestButton.backgroundColor = UIColor(hexString:rightbright)
        startTestButton.addTarget(self, action: "startTestButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        startTestButton.hidden = true
        self.view.addSubview(startTestButton)
        
        addWords()
        changeLanguageModel()
        setUpLabels()
        sayCurrentInstructions()
    }
    
    // Used for resetting/ formatting labels to initial state
    func setUpLabels() {
        testTitle?.text = balanceTestDictionary[testIndex]
        numOfErrors?.text = "0"
        timeLabel?.text = "20"
        
        testTitle?.sizeToFit()
        numOfErrors?.sizeToFit()
        timeLabel?.sizeToFit()
        
        testTitle?.center = CGPoint(x: xloc, y:yloc)
        numOfErrorsHeader?.center = CGPoint(x: xloc, y:yloc*2)
        numOfErrors?.center = CGPoint(x: xloc, y:yloc*3)
        timeLabelHeader?.center = CGPoint(x: xloc, y:yloc*4)
        timeLabel?.center = CGPoint(x: xloc, y:yloc*5)
    }
    
    // used for case when user completes JUST balance test so they can see score
    func resultsOfBalanceTestView () {
        // set up the labels for the score screen
        testTitle?.text = "Balance Test Results"
        numOfErrorsHeader?.text = "Total number of Errors: (max 10)"
        numOfErrors?.text = String(GlobalVariables.sharedInstance.balanceTestScore)
        timeLabelHeader?.text = ""
        timeLabel?.text = ""
        
        testTitle?.sizeToFit()
        numOfErrorsHeader?.sizeToFit()
        numOfErrors?.sizeToFit()
        
        testTitle?.center = CGPoint(x: xloc, y:yloc)
        numOfErrorsHeader?.center = CGPoint(x: xloc, y:yloc*2)
        numOfErrors?.center = CGPoint(x: xloc, y:yloc*3)
        
        // enable return home button
        startTestButton.hidden = false
        startTestButton.setTitle("Return Home", forState: UIControlState.Normal)
        startTestButton.backgroundColor = UIColor(hexString: "#a85ac9")
    }
    
    // reset all scene state variables
    func resetSceneVariables() {
        errors = 0
        firstGyroIteration = false
        initialStateGyroValue = 0.0
        counter = 0
        timeOfLastErr = 0
    }
    
    // used to distinguish between which balance test we are on
    func sayCurrentInstructions() {
        switch testIndex {
        case 0:
            voiceCommand("This is the balance test --- the first stance is the double leg stance. -- standing with your feet together -- and with your eyes closed -- You should hold the phone in front of you --  You should try to maintain stability in that position for 20 seconds -- I will be counting the number of times you move out of this position -- Please push the start button -- or  say start -- when you are ready")
        case 1:
            voiceCommand("Next is the Single Leg Stance -- If you were to kick a ball -- This will be with your dominant foot -- Stand on your non-dominant foot. --- The dominant leg should be held in approximately thirty degrees of hip flexion --- and 45 degrees of knee flexion. -- Again, you should try to maintain stability for twenty seconds with the phone in front of you --- and your eyes closed. --- I will be counting the number of times you move out of this position. ---  If you stumble out of this position --- open your eyes --- return to the start position and continue balancing. --- I will start timing when you are set - Please push the start button -- or  say start -- when you are ready")
        case 2:
            voiceCommand("Next is the Tandem Stance -- Please stand heel to toe -- with your non-dominant foot in back --- Your weight should be evenly distributed - across both feet --- Again, you should try to maintain stability for twenty seconds with the phone in front of you --- and your eyes closed. --- I will be counting the number of times you move out of this position. ---  If you stumble out of this position --- open your eyes --- return to the start position and continue balancing. --- I will start timing when you are set - please say start -- or push the start button -- when you are ready")
        default:
            testIndex = 0
            testDone = true
            // normal SCAT3 test => go to delayed recall
            if (GlobalVariables.sharedInstance.delayedRecallFlag == 1) {
                
                performSegueWithIdentifier("DelayedRecallSegue", sender: self)
            }
                // independent balance test
            else {
                resultsOfBalanceTestView()
            }
            break
        }
    }
    
    // event is fired AT THE END OF THE VOICE DICTATION
    override func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didFinishSpeechUtterance utterance: AVSpeechUtterance!) {
        // enable the balance test start button
        startTestButton.hidden = false
        resumeRecognition()
    }
    
    // starts the balance test / intializes the NSTimer / intializes the gyroscope
    func startBalanceTest() {
        
        // USED TO BE .7
        motionManager.gyroUpdateInterval = 0.5
        motionManager.startGyroUpdatesToQueue(NSOperationQueue.currentQueue(), withHandler: { (gyroData: CMGyroData!, error: NSError!) -> Void
            in
            self.outputGyroData(gyroData.rotationRate)
            if (error != nil) {
                println("\(error)")
            }
        })
        
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("updateCounter"), userInfo: nil, repeats: true)
    }
    
    func updateCounter() {
        // stop timer at 20 / current test is over
        if(counter == 20) {
            motionManager.stopGyroUpdates()
            timer.invalidate()
            
            // record each of the errors in our global variable for score screen
            if(testIndex < 3) {
                GlobalVariables.sharedInstance.balanceTestScore += errors
            }
            
            // increment the test index and start the next test
            if(testIndex <= 3) {
                testIndex++;
                setUpLabels()
                resetSceneVariables()
                sayCurrentInstructions()
            }
        }
        else {
            // update timer label on scene
            timeLabel?.text = "\(20 - counter++)"
            timeLabel?.sizeToFit()
            timeLabel?.center = CGPoint(x: xloc, y:yloc*5);
        }
    }
    
    // delegate method for the Gyroscope / updates Number of Errors Label on view
    func outputGyroData(rotation: CMRotationRate) {
        // get the avg of the gyro data
        let gyroAvg  = (rotation.x + rotation.y + rotation.z) / 3;
        
        // don't record the first iteration, we need to get initial state
        if (!firstGyroIteration) {
            firstGyroIteration = true
            initialStateGyroValue = gyroAvg
            return
        }
        
        // only record errors if its greater than .15 threshold | the errors aren't within 1 second interval | Number of errors are less than 10
        if (abs(gyroAvg - initialStateGyroValue) > 0.15 &&  counter > (timeOfLastErr + 2 ) && errors < 10) {
            errors++
            timeOfLastErr = counter
            numOfErrors?.text = "\(errors)"
            numOfErrors?.sizeToFit()
            numOfErrors?.center = CGPoint(x: xloc, y:yloc*3)
        }
    }
    
    // method receives data String from voice recognition, indexes into dictionary to receive reference to UIButton
    override func pocketsphinxDidReceiveHypothesis(hypothesis: String, recognitionScore: String, utteranceID: String) {
        println(hypothesis)
        if(hypothesis == "START") {
            startTestButton.hidden = true
            startBalanceTest()
            suspendRecognition()
        }
    }
    
    // on scene change perform clean up actions
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        suspendRecognition()
        removeWords()
        removeButtons()
        resetSceneVariables()
        self.openEarsEventsObserver.delegate = nil
        self.openEarsEventsObserver = nil
    }
    
    override func addWords() {
        words.append("START")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

