//
//  YearOrientationController.swift
//  concussion-app
//
//  Created by Michael Banzon on 5/21/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit

class YearOrientationController: OpenEarsViewController {
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var delButton: UIButton!
    @IBOutlet var keypad: [UIButton] = []
    
    @IBOutlet var year: UILabel!
    var yearEntered: String = ""
    
    var keypadStrings: [String] = ["1","2","3","4","5","6",
        "7","8","9","0","DEL"];
    
    //digitsPressed on the keypad
    var digitsPressed = 0
    
    @IBAction func buttonPress(sender : UIButton) {
        
        var digittext = sender.titleLabel!.text!
        placeDigit(digittext)
    }
    
    @IBAction func delButtonPress(sender : UIButton) {
        removeDigit()
    }
    
    func placeDigit(digittext: NSString)
    {
        if(count(yearEntered) < 4) {
            yearEntered = yearEntered + (digittext as String)
            year.text = yearEntered
        }
    }
    
    func removeDigit()
    {
        var length = count(yearEntered)
        if length > 0 {
            yearEntered = yearEntered.substringToIndex(advance(yearEntered.startIndex, length - 1))
        }
        year.text = yearEntered
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup navigation bar title
        self.navigationItem.title = "Orientation Assessment"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        nextButton.enabled = false
        // Text-To-Speech command for test
        voiceCommand("Please enter the current year")
        setupView()
    }
    
    override func viewDidAppear(animated: Bool) {
        addWords()
        changeLanguageModel()
    }
    
    func setupView() {
        //border dimensions and measurements
        let ypad_top : CGFloat = screenHeight * 0.15
        let ypad_bot : CGFloat = screenHeight * 0.35
        let xpad_left : CGFloat = screenWidth * 0.10
        let xpad_right : CGFloat = screenWidth * 0.10
        
        //used to position the digits on the screen
        var numDigits:CGFloat = 1.0
        
        let celldigx : CGFloat = (screenWidth - xpad_left - xpad_right)/numDigits
        let celldigy : CGFloat = (screenHeight - ypad_top - ypad_bot)
        
        //used to position the keypad on the screen
        let cellkeyx : CGFloat = (screenWidth - xpad_left - xpad_right)/3.0
        let cellkeyy : CGFloat = (0.80)*ypad_bot / 4.0
        
        //width & height of the buttons
        let keypadw :CGFloat = cellkeyx - 4
        let keypadh :CGFloat = cellkeyy - 4
        
        let digitw : CGFloat = 58
        let digith : CGFloat = 64
        
        //font size variables
        var digitFontSize : CGFloat = 32
        var keypadFontSize: CGFloat = 18
        var titleFontSize : CGFloat = 24
        
        var header = UILabel(frame: CGRectMake(0, 0, 280, 45))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 36)
        header.layer.cornerRadius = 5
        header.layer.masksToBounds = true
        header.text = "Date and Time"
        self.view.addSubview(header)
        
        //insert first question
        var q2 = UILabel(frame: CGRectMake(0, 0, 280, 45))
        q2.center = CGPointMake((screenWidth/2), (screenHeight/4))
        q2.textAlignment = NSTextAlignment.Center
        q2.font = UIFont(name: q2.font.fontName, size: 20)
        q2.layer.cornerRadius = 5
        q2.layer.masksToBounds = true
        q2.text = "What year is it?"
        self.view.addSubview(q2)
        
        //generate the digits for this trial
        var i = 0
        var row :CGFloat = 0
        var col :CGFloat = 0
        for key in keypad
        {
            var xpos: CGFloat = xpad_left + cellkeyx*col + cellkeyx/2.0
            var ypos: CGFloat = screenHeight - ypad_bot + cellkeyy*row + cellkeyy/2.0
            key.frame = CGRectMake(xpos,ypos,keypadw,keypadh)
            key.center = CGPoint(x: xpos, y: ypos)
            key.setTitle(keypadStrings[i], forState: UIControlState.Normal)
            key.titleLabel!.font = UIFont(name: key.titleLabel!.font.fontName, size: keypadFontSize)
            key.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            key.layer.borderColor = UIColor.blackColor().CGColor
            key.layer.borderWidth = 1.5
            key.backgroundColor = UIColor(hexString: buttonColor)
            col++
            if(col == 3)
            {
                row++
                col=0
            }
            i++
        }
        
        var xpos: CGFloat = xpad_left + celldigx*CGFloat(0) + celldigx/2.0
        var ypos: CGFloat = ypad_top + celldigy/2.0
        year.frame = CGRectMake(xpos,ypos,300, 75)
        year.center = CGPoint(x: xpos,y: ypos)
        year.font = UIFont(name: year.font.fontName, size: 75)
    }
    
    // event is fired AT THE END OF THE VOICE DICTATION
    override func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didFinishSpeechUtterance utterance: AVSpeechUtterance!) {
        nextButton.enabled = true
        resumeRecognition()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
    *  BEGIN VOICE RECOGNITION METHODS
    */
    
    // method to build language model for scene
    override func addWords() {
        words.append("0")
        words.append("1")
        words.append("2")
        words.append("3")
        words.append("4")
        words.append("5")
        words.append("6")
        words.append("7")
        words.append("8")
        words.append("9")
        words.append("NEXT")
    }
    
    // voice recognition receiver method
    override func pocketsphinxDidReceiveHypothesis(hypothesis: String, recognitionScore: String, utteranceID: String) {
        let numtext = hypothesis.componentsSeparatedByString(" ")[0]
        if(hypothesis == "NEXT") {
            performSegueWithIdentifier("YearTestSegue", sender: self)
        }
        else {
            placeDigit(numtext as NSString)
        }
    }
    
    // checks if the currently selected year is correct
    func checkAnswer() {
        let ans:Int? = year.text?.toInt()
        if(ans == currYear) {
            println("correct year")
            GlobalVariables.sharedInstance.orientationScore++
        }
    }
    
    
    // on scene change perform clean up actions
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        suspendRecognition()
        checkAnswer()
        removeWords()
        removeButtons()
        self.openEarsEventsObserver.delegate = nil
        self.openEarsEventsObserver = nil
    }
    
    /**
    *  END VOICE RECOGNITION METHODS
    */
}
