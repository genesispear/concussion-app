//
//  wordMemory.swift
//  concussion-app
//
//  Created by Alex Rumbaugh on 2/21/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import Foundation

class digitMemoryClass {
    
    
    //number of test iterations that will be run
    var totalTrials = 3
    
    //current trial (0 based)
    var currentTrial = 0
    
    //digits chosen for a particular trial
    var chosenDigits :[String] = []
    
    
    //number of digits displayed for a given trial
    var digitsPerTrial = [3,4,5,6]
    
    var digitsPressed = 0
    var correctDigits = 0
    
    func generateDigits() {
        chosenDigits.removeAll(keepCapacity: true)
        for num in 1...digitsPerTrial[currentTrial]
        {
            let randomNumber = arc4random_uniform(UInt32(9))
            chosenDigits.append(String(randomNumber))
        }

        if (chosenDigits[0] == chosenDigits[digitsPerTrial[currentTrial]-1])
        {
            let firstDigit = chosenDigits[0].toInt()
            chosenDigits[0] = String( firstDigit! + 1 )
        }

    }
}
