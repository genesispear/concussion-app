//
//  NV_ReverseMonthsTestController.swift
//  concussion-app
//
//  Created by Nathaniel Ho on 6/3/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit

class NV_ReverseMonthsController: UIViewController {
    @IBOutlet var labelArray: [UILabel] = []
    var nextButton: UIBarButtonItem!
    var rightButton: UIButton!
    var wrongButton: UIButton!
    
    var score: Int = 1
    
    var months: [String] = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE",
        "JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set Up the Nav Bar
        self.navigationItem.title = "Memory Assessment"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        nextButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: "nextAction")
        navigationItem.rightBarButtonItem = nextButton
        nextButton.enabled = false;
        
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        //insert Header "Date and Time"
        var header = UILabel(frame: CGRectMake(0, 0, screenWidth, 60))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 40)
        header.layer.cornerRadius = 0
        header.layer.masksToBounds = true
        header.text = "Month Reversal"
        
        self.view.addSubview(header)
        
        //insert first question
        var q1 = UILabel(frame: CGRectMake(0, 0, screenWidth, 50))
        q1.center = CGPointMake((screenWidth/2), (screenHeight/4))
        q1.textAlignment = NSTextAlignment.Center
        q1.font = UIFont(name: q1.font.fontName, size: 20)
        q1.layer.cornerRadius = 0
        q1.layer.masksToBounds = true
        q1.text = "State months in reverse order"
        self.view.addSubview(q1)
        
        labelLayout()
        
        //place the right and wrong buttons
        let rwbuttonheight = screenWidth/(2*1.6)
        let ypos : CGFloat = screenHeight - rwbuttonheight
        
        rightButton = UIButton(frame: CGRectMake(0,ypos,screenWidth/2, rwbuttonheight))
        rightButton.setTitle("Right", forState: UIControlState.Normal)
        rightButton.backgroundColor = UIColor(hexString:rightfade)
        rightButton.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(rightButton)
        
        wrongButton = UIButton(frame: CGRectMake(screenWidth/2,ypos,screenWidth/2, rwbuttonheight))
        wrongButton.setTitle("Wrong", forState: UIControlState.Normal)
        wrongButton.backgroundColor = UIColor(hexString:wrongfade)
        wrongButton.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(wrongButton)
        
    }
    
    func labelLayout() {
        var ypad : CGFloat = 0.3 * screenHeight;
        var xpad : CGFloat = 0.10 * screenWidth;
        var xcell : CGFloat;
        var ycell : CGFloat;
        xcell = screenWidth
        xcell = (screenWidth - 2.0*xpad) / 2.0
        ycell = (screenHeight - 1.7*ypad) / 6.0
        var i = 0
        var row : CGFloat = 0.0;
        var col : CGFloat = 0.0;
        for label in labelArray {
            label.frame = CGRectMake(screenWidth*(0.15+row), screenHeight*(1/6+col), 108, 32)
            label.text = months[11-i]
            var xpos = xpad + xcell * col + xcell / 2.0
            var ypos = ypad + ycell * row + ycell / 2.0
            label.center = CGPoint(x: xpos, y: ypos)
            label.textAlignment = NSTextAlignment.Center
            label.font = UIFont(name: label.font.fontName, size: 17)
            label.layer.cornerRadius = 0
            label.layer.masksToBounds = true
            
            if(i == 5) {
                col++;
                row = 0;
            }
            else {
                row++;
            }
            
            i++
        }
    }
    
    func buttonAction(sender: UIButton) {
        if(sender == rightButton) {
            score = 1
            nextButton.enabled = true
            wrongButton.backgroundColor = UIColor(hexString:wrongfade)
            sender.backgroundColor = UIColor(hexString:rightbright)
        }
        else { // sender == wrongButton
            score = 0
            nextButton.enabled = true
            rightButton.backgroundColor = UIColor(hexString:rightfade)
            sender.backgroundColor = UIColor(hexString:wrongbright)
        }
    }
    
    func nextAction() {
        println("\(score)")
        GlobalVariables.sharedInstance.concentrationScore += score
        performSegueWithIdentifier("NVMonthReversalTestSegue", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}