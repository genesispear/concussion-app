//
//  SymEvalController.swift
//  concussion-app
//
//  Created by Nathaniel Ho on 2/21/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit
import AVFoundation

class SymEvalController: OpenEarsViewController {
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var buttonarray: [UIButton]!
        
    var questionLabel: UILabel!
//    var micStatus: UILabel!
    var skipButton: UIButton!
    var symTotal: Int = 0 // Total number of symptoms (Maximum possible 22)
    var symSeverityScore: Int = 0 // Symptom severity score (Maximum possible 132)
    var currentElm: Int = 0
    var questionVals: [Int] = Array(count:22, repeatedValue: 0)
    // total 22 items
    var questionItems: [String] = [
        "Headache",
        "\"Pressure in head\"",
        "Neck Pain",
        "Nausea or vomiting",
        "Dizziness",
        "Blurred vision",
        "Balance problems",
        "Sensitivity to light",
        "Sensitivity to noise",
        "Feeling slowed down",
        "Feeling like \"in a fog\"",
        "\"Don't feel right\"",
        "Difficulty concentrating",
        "Difficulty remembering",
        "Fatigue or low energy",
        "Confusion",
        "Drowsiness",
        "Trouble falling asleep",
        "More emotional",
        "Irritability",
        "Sadness",
        "Nervous or Anxious"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        
        var header = UILabel(frame: CGRectMake(0, 0, 280, 45))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 26)
        header.layer.cornerRadius = 5
        header.layer.masksToBounds = true
        header.text = "How are you feeling?"
        
        self.view.addSubview(header)
        
        questionLabel = UILabel(frame: CGRectMake(0, 0, 280, 45))
        questionLabel.center = CGPointMake((screenWidth/2), (screenHeight/4))
        questionLabel.textAlignment = NSTextAlignment.Center
        
        questionLabel.font = UIFont(name: questionLabel.font.fontName, size: 20)
        questionLabel.layer.cornerRadius = 5
        questionLabel.layer.masksToBounds = true
        questionLabel.text = questionItems[0]
        
        self.view.addSubview(questionLabel)

//        micStatus = UILabel(frame: CGRectMake(0, 0, 65.0, 60.0))
//        micStatus.center = CGPointMake(screenWidth/2.0, screenHeight*(7/8))
//        micStatus.clipsToBounds = true
//        micStatus.backgroundColor = UIColor.greenColor()
//        micStatus.layer.cornerRadius = 100/4.0
//        micStatus.text = "Mic Off"
//        micStatus.textAlignment = NSTextAlignment.Center
//        
//        self.view.addSubview(micStatus)
        
        nextButton.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        
        // FOR DEVELOPMENT PURPOSES, DELETE IN FINISHED PRODUCT
        // add button for skipping to last question in SymEval section
//        skipButton = UIButton(frame: CGRectMake(0, 0, 250, 45))
//        skipButton.setTitle("Skip to Last Question", forState: UIControlState.Normal)
//        skipButton.center = CGPointMake((screenWidth * 0.5), (screenHeight * 0.65))
//        skipButton.layer.borderColor = UIColor.blackColor().CGColor
//        skipButton.layer.borderWidth = 2.0
//        skipButton.clipsToBounds = true
//        skipButton.backgroundColor = UIColor.purpleColor()
//        skipButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
//        skipButton.addTarget(self, action: "skipButtonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        
//        self.view.addSubview(skipButton)
        
        // initially disable nextButton because no answer has been entered
        nextButton.enabled = false
        
        self.buttonLayout()
        
        // Text-To-Speech command for test
        voiceCommand("Please score yourself on the following symptoms, based on how you feel now. \(questionLabel.text!)")
        
        //start voice recognition
        
        
        addWords()
        changeLanguageModel()
    }
    
    // FOR DEVELOPMENT PURPOSES, DELETE IN FINISHED PRODUCT
    // add button for skipping to last question in SymEval section
//    func skipButtonAction(sender: UIButton!) {
//        println("skipping!")
//        currentElm = 20
//        questionLabel.text = questionItems[++currentElm]
//        nextButton.enabled = false
//        suspendRecognition()
//        for button in buttonarray {
//            button.backgroundColor = UIColor.whiteColor()
//        }
//        voiceCommand(questionLabel.text!)
//    }
    
    // event is fired AT THE END OF THE VOICE DICTATION
    override func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didFinishSpeechUtterance utterance: AVSpeechUtterance!) {
        for button in buttonarray {
            button.enabled = true
        }
        resumeRecognition()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func buttonLayout() {
        
        var i = 0
        var row : CGFloat = 0.0;
        var col : CGFloat = 0.0;
        var xpad : CGFloat = 0.05 * screenWidth
        var xcell : CGFloat = (screenWidth - 2 * xpad) / 7.0
        
        for button in buttonarray {
            buttonArray["\(i)"] = button
            button.setTitle("\(i)", forState: UIControlState.Normal)
            button.frame = CGRectMake(0, 0, xcell-4, 1.6*(xcell-4))
            
            let xpos = xpad + row*xcell + xcell / 2.0
            let ypos = 0.5 * screenHeight
            
            button.center = CGPoint(x:xpos,y:ypos)
            
            button.layer.borderColor = UIColor.blackColor().CGColor
            button.layer.borderWidth = 0.5
            button.clipsToBounds = true
            button.backgroundColor = UIColor(hexString: buttonColor)
            button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
            //button.layer.cornerRadius = 100/2.0
            button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            button.titleLabel!.font = UIFont(name: button.titleLabel!.font.fontName, size: 24)
            row++
            i++;
        }
        buttonArray["NEXT"] = nextButton
    }
    
    func buttonAction(sender:UIButton!) {
        // move increment/decrement index in questionItems array
        // depending on which button was pressed
        if(sender == nextButton) {
            // currentElm == index of last set of questions
            // clicking "Next" should push new screen instead of reloading data
            if(currentElm == questionItems.count - 1) {
                // calculate sums
                symTotal = questionVals.reduce(0) {
                    if($1 > 0) {
                        return $0 + 1
                    }
                    return $0
                }
                symSeverityScore = questionVals.reduce(0) {
                    return $0 + $1
                }
                // send data to next screen, push next screen
                GlobalVariables.sharedInstance.symTotal = self.symTotal
                GlobalVariables.sharedInstance.symSeverityScore = self.symSeverityScore
                performSegueWithIdentifier("nextSymEval", sender: nil)
                return
            }
            questionLabel.text = questionItems[++currentElm]
            nextButton.enabled = false
            suspendRecognition()
            for button in buttonarray {
                button.backgroundColor = UIColor(hexString: buttonColor)
            }
            voiceCommand(questionLabel.text!)
        }
        
        else {
            //change color
            changeButtonColor(sender)
            
            for button in buttonarray {
                if button != sender {
                    button.backgroundColor = UIColor(hexString: buttonColor)
                }
            }
            questionVals[currentElm] = sender.titleLabel!.text!.toInt()!

            nextButton.enabled = true
        }
    }
    
    func changeButtonColor(button: UIButton){
        if(button.backgroundColor == UIColor(hexString: buttonColor)){
            button.backgroundColor = UIColor(hexString: buttonPressed)
        }
        else{
            button.backgroundColor = UIColor(hexString: buttonColor)
        }
    }
    
    /**
    *  BEGIN VOICE RECOGNITION METHODS
    */
    
    
    // method receives data String from voice recognition, indexes into dictionary to receive reference to UIButton
    override func pocketsphinxDidReceiveHypothesis(hypothesis: String, recognitionScore: String, utteranceID: String) {
        let selBtnArray = hypothesis.componentsSeparatedByString(" ")
        let selBtnString = (selBtnArray[0] as NSString)
        let selBtnRef = buttonArray[selBtnString as String]!
        
        // if answer is selected, disable voice commmands to changing the answer
        // if no answer is selected, disable voice commands for NEXT
        if(nextButton.enabled == false && selBtnString == "NEXT") {
            return
        }
        
        buttonAction(selBtnRef)
    }

//    override func pocketsphinxDidStartListening() {
//        micStatus.backgroundColor = UIColor.redColor()
//        micStatus.text = "Mic On"
//    }
//    
//    override func pocketsphinxDidResumeRecognition() {
//        micStatus.backgroundColor = UIColor.redColor()
//        micStatus.text = "Mic On"
//    }
//    
//    override func pocketsphinxDidSuspendRecognition() {
//        micStatus.backgroundColor = UIColor.greenColor()
//        micStatus.text = " Mic Off"
//    }
    
    // on exit of scene stop listening
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        suspendRecognition()
        removeWords()
        removeButtons()
        self.openEarsEventsObserver.delegate = nil
        self.openEarsEventsObserver = nil
        
    }
    
    override func addWords() {
        words.append("0")
        words.append("1")
        words.append("2")
        words.append("3")
        words.append("4")
        words.append("5")
        words.append("6")
        words.append("NEXT")
    }
    
    /**
    *  END VOICE RECOGNITION METHODS
    */
}

