//
//  digitMemory_title.swift
//  concussion-app
//
//  Created by Alex Rumbaugh on 3/1/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit

class maddocksTestViewController: UIViewController, UIScrollViewDelegate {
    
    var wrongfade = "#e9bdca"
    var wrongbright = "#c95a7b"
    
    var rightfade = "#cae9bd"
    var rightbright = "#7bc95a"
    
    
    var scrollView: UIScrollView!
    var containerView = UIView()
    
    //border dimensions and measurements
    var bounds: CGRect = UIScreen.mainScreen().bounds
    var width:CGFloat = 0
    var height:CGFloat = 0
    var ypad_top : CGFloat = 0
    var ypad_bot : CGFloat = 0
    var xpad_left : CGFloat = 0
    var xpad_right : CGFloat = 0
    var headerheight: CGFloat = 0
    var answerheight : CGFloat = 0
    var questionheight : CGFloat = 0
    
    
    
    
    var questions: [UILabel] = []
    
    var rightbuttons: [UIButton] = []
    var wrongbuttons: [UIButton] = []
    
    var score = 0
    
    var question_strings: [String] = [" What venue are we at today?", " Which half is it now?", " Who scored last in this match?", " What team did you \nplay last week/game?", " Did your team win the last game?"]
    
    
    var pressed: [Int] = [-1,-1,-1,-1,-1]
    
    
    //the buttons the user has selected
    var buttonsPressed: [Int] = [-1,-1,-1,-1,-1]
    

    
    func pressWrong(sender : UIButton) {
        
        let i = sender.tag
        if(pressed[i] != 1)
        {
            rightbuttons[i].backgroundColor = UIColor(hexString:rightfade)
            sender.backgroundColor = UIColor(hexString:wrongbright)
            pressed[i] = 1
        }

    }
    
    func pressRight(sender : UIButton) {
        
        let i = sender.tag
        if(pressed[i] != 0)
        {
            wrongbuttons[i].backgroundColor = UIColor(hexString:wrongfade)
            sender.backgroundColor = UIColor(hexString:rightbright)
            pressed[i] = 0
        }
        
    }
    
    
    
    
    func calculateScore()
    {
        score = 0
        for num in pressed
        {

            score += num
        }
    }
    
    func leaveScene()
    {
        println("leaving scene")
        calculateScore()
        GlobalVariables.sharedInstance.maddocksScore = score
        println(score)
        performSegueWithIdentifier("nextScene", sender:self)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup navigation bar title
        self.navigationItem.title = "Maddocks Test"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        var nextButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: "leaveScene")
        navigationItem.rightBarButtonItem = nextButton
        //nextButton.enabled = false;
        
        var navbounds = self.navigationController?.navigationBar.bounds
        var navheight1 = navbounds?.height
        var navheight : CGFloat = CGFloat(navheight1!) + 20.0
        //var navheight: CGFloat = UIApplication.sharedApplication().statusBarFrame.size.height
        println(navheight)
        
        width = bounds.size.width
        height = bounds.size.height
        
        ypad_top = height * 0.15
        ypad_bot = height * 0.1
        xpad_left = width * 0.02
        xpad_right = width * 0.02
        headerheight = GlobalVariables.sharedInstance.headerheight
        answerheight = GlobalVariables.sharedInstance.answerheight
        questionheight = GlobalVariables.sharedInstance.questionheight
       
        
        
        //create the scrollView
        scrollView = UIScrollView()
        self.scrollView.delegate = self
        scrollView.backgroundColor = UIColor(hexString: bgColor)
        //scrollView.backgroundColor = UIColor.redColor()
        scrollView.contentSize = CGSize(width: screenWidth, height: questionheight)
        scrollView.frame = CGRectMake(0,navheight + headerheight,screenWidth, screenHeight)
        //scrollView.alwaysBounceVertical = false
        self.view.addSubview(scrollView)
        

        
        //font size variables
        var digitFontSize : CGFloat = 32
        var keypadFontSize: CGFloat = 18
        var titleFontSize : CGFloat = 24
        
        
        //insert instructions
        var q2 = UILabel(frame: CGRectMake(0, navheight, screenWidth, headerheight))
        //q2.center = CGPointMake((screenWidth/2), navheight! + headerheight/2.0)
        q2.textAlignment = NSTextAlignment.Center
        q2.font = UIFont(name: q2.font.fontName, size: 16)
        q2.layer.masksToBounds = true
        q2.text = "Ask the following questions"
        q2.backgroundColor = UIColor(hexString: bgColor)
        self.view.addSubview(q2)
        
        
        //place the question labels
        var row :CGFloat = 0
        var col :CGFloat = 0
        var qcellx : CGFloat = screenWidth
        var qcelly : CGFloat = ((questionheight) / 8.0)
        var qwidth: CGFloat = screenWidth
        var qheight: CGFloat = qcelly / 2.5
        var qFontSize: CGFloat = 16
        for i in 0...question_strings.count-1
        {
            print(i)
            var xpos: CGFloat = qcellx/2.0
            var ypos: CGFloat = qcelly*row
            var newlabel = UILabel(frame: CGRectMake(0,ypos,qwidth,qheight))
            
            newlabel.font = UIFont(name: newlabel.font.fontName, size: qFontSize)
            newlabel.text = question_strings[i]
            newlabel.textColor = UIColor.blackColor()
            newlabel.backgroundColor = UIColor(hexString:"#d3ace4")
            newlabel.layer.borderWidth = 1.0
            newlabel.numberOfLines = 0;
            newlabel.textAlignment = NSTextAlignment.Center;
            //newlabel.sizeToFit()
            //newlabel.center = CGPoint(x: xpos, y: ypos)
            questions.append(newlabel)
            scrollView.addSubview(questions[i])
            
            var rightB = UIButton(frame: CGRectMake(0,ypos+qheight,qwidth/2.0,1.5*qheight))
            rightB.setTitle("Right", forState: UIControlState.Normal)
            rightB.backgroundColor = UIColor(hexString:rightfade)
            rightB.tag = i
            rightB.addTarget(self, action: "pressRight:", forControlEvents: UIControlEvents.TouchUpInside)
            rightbuttons.append(rightB)
            scrollView.addSubview(rightbuttons[i])
            
            var wrongB = UIButton(frame: CGRectMake(screenWidth/2.0,ypos+qheight,qwidth/2.0,1.5*qheight))
            wrongB.setTitle("Wrong", forState: UIControlState.Normal)
            wrongB.backgroundColor = UIColor(hexString:wrongfade)
            wrongB.tag = i
            wrongB.addTarget(self, action: "pressWrong:", forControlEvents: UIControlEvents.TouchUpInside)
            wrongbuttons.append(wrongB)
            scrollView.addSubview(wrongbuttons[i])
            row++
        }
        
        //create the answer buttons
        //prepareNextQuestion(-1)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
