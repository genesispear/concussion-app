//
//  NV_BalanceDescriptionsController.swift
//  concussion-app
//
//  Created by Michael Banzon on 6/1/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import Foundation
import UIKit

class NVBalanceDescriptionsController: UIViewController {
    
    @IBOutlet var testTitle: UILabel!
    @IBOutlet var descriptions: UILabel!
    
    //move to the balance test scene
    @IBOutlet var readyButton: UIButton!
    
    // index specifies which balance test we are on
    let balanceTest:[Int:String] = [0:"Double Leg Stance", 1:"Single Leg Stance", 2:"Tandem Stance", 3: "Finished"];
    
    //balance instruction strings
    let double: String = "Have the patient stand with his/her feet together and with his/her eyes closed holding the phone in front of him/her.\n \n Press READY when the patient is in position."
    
    let single: String = "Have the patient stand on his/her non-dominant foot (NOT the foot he/she would kick a ball with).\n \nThe dominant leg should be held in approximately 30 degrees of hip flexion and 45 degrees of knee flexion.\n \nThe patient should have his/her eyes closed holding the phone in front of him/her.\n \nPress READY when the patient is in position."
    
    let tandem: String = "Have the patient stand heel to toe with his/her non-dominant foot in back.\n \n His/Her weight should be evenly distributed across both feet.\n \nThe patient should have his/her eyes closed holding the phone in front of him/her.\n \n Press READY when the patient is in position."
    
    
    // used for placement of UILabels
    var xloc = screenWidth / 2.0;
    var yloc = screenHeight / 7.0 + 5.0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let testDespcriptions:[Int:String] = [0:self.double, 1:self.single, 2:self.tandem];
        
        //setup navigation bar title
        self.navigationItem.title = "Balance Test"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        // sets up the background color
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        // ready button
        readyButton = UIButton(frame: CGRectMake(0,screenHeight * 0.85,screenWidth,screenHeight * 0.15 ))
        readyButton.setTitle("READY", forState: UIControlState.Normal)
        readyButton.backgroundColor = UIColor(hexString:rightbright)
        readyButton.addTarget(self, action: "beginTest", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(readyButton)
        
        //testTitle
        testTitle?.text = balanceTest[balanceTestIndex]
        testTitle.font = testTitle.font.fontWithSize(36.0)
        testTitle?.sizeToFit()
        testTitle?.center = CGPoint(x: xloc + 2, y:yloc+3)
        
        //let size = CGSizeMake(xloc-4, yloc/2)
        //descriptions
        descriptions = UILabel(frame: CGRectMake(5, screenHeight*0.22, screenWidth-5, screenHeight*(1/2)))
        //descriptions?.center = CGPoint(x: xloc, y:yloc*3.5)
        descriptions?.text = testDespcriptions[balanceTestIndex]
        descriptions.font = descriptions.font.fontWithSize(20.0)
        descriptions.backgroundColor = UIColor(hexString: buttonColor)
        descriptions.numberOfLines = 0;
        descriptions?.sizeToFit()
        self.view.addSubview(descriptions)
    }
    
    func beginTest() {
        performSegueWithIdentifier("NVBalanceDescriptionSegue", sender: self)
    }
}

