//
//  ViewController.swift
//  concussion-app
//
//  Created by Nathaniel Ho on 2/16/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit

class wordMemoryTitleViewController: OpenEarsViewController {

    @IBOutlet var buttonarray: [UIButton] = []
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var instructionsLabel: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //setup navigation bar title
        self.navigationItem.title = "Cognitive Assessment"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton

        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        
        let ypad_top : CGFloat = screenHeight * 0.30
        let ypad_bot : CGFloat = screenHeight * 0.25
        let xpad_left : CGFloat = screenWidth * 0.05
        let xpad_right : CGFloat = screenWidth * 0.05
        
        let buttonw :CGFloat = 120
        let buttonh :CGFloat = 32
        
        var rownum : CGFloat = 0
        var colnum : CGFloat = 0
        var xpos: CGFloat = 0
        var ypos: CGFloat = 0
        
        
        // Insert header for word memory test
        var header = UILabel(frame: CGRectMake(0, 0, screenWidth, 45))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 36)
        header.layer.cornerRadius = 5
        header.layer.masksToBounds = true
        header.text = "Immediate Memory"
        self.view.addSubview(header)
        
        //insert sub header
        var q2 = UILabel(frame: CGRectMake(0, 0, 280, 45))
        q2.center = CGPointMake((screenWidth/2), (screenHeight/4))
        q2.textAlignment = NSTextAlignment.Center
        q2.font = UIFont(name: q2.font.fontName, size: 20)
        q2.layer.cornerRadius = 5
        q2.layer.masksToBounds = true
        q2.text = "Remember the following words"
        self.view.addSubview(q2)
        
        var ycell = (screenHeight - 1.7*ypad_top) / 4.0
        
        //only generate words the first time
        if wordMemoryTestInst.currentIteration == 0
        {
            wordMemoryTestInst.generateWords()
        }
        
        var i = 0
        var row :CGFloat = 0
        
        for button in buttonarray
        {
            button.setTitle(wordMemoryTestInst.chosenWords[i], forState: UIControlState.Normal)
            button.frame = CGRectMake(0, 0, buttonw, buttonh)
            button.layer.borderColor = UIColor.whiteColor().CGColor
            button.backgroundColor = UIColor(hexString: bgColor)
            button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            var loc = CGPointMake(screenWidth / 2.0, ypad_top + row*ycell + ycell / 2.0)
            button.center = loc
            i++
            row++
        }
        
        //nextButton.enabled = false
        nextButton.hidden = true
        
        // construct string for voice command
        var wordSpeech = "-- "
        for word in wordMemoryTestInst.chosenWords
        {
            wordSpeech += word
            wordSpeech += ". -- "
        }
        
        if (wordMemoryTestInst.currentIteration == 0)
        {
            voiceCommand("I am go ing to test your memory -- I will read you a list of words  -- and when I am done  -- repeat back as many words as you can remember  -- in whatever order." + wordSpeech)
        }
        else
        {
            voiceCommand("I am go ing to repeat the same list again -- Repeat back -- as many words as you can remember -- in whatever order -- even if you said the word before." + wordSpeech)
        }
        
        //start voice recognition
        addWords()
        changeLanguageModel()
    }
    
    // event is fired AT THE END OF THE VOICE DICTATION
    override func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didFinishSpeechUtterance utterance: AVSpeechUtterance!) {
        resumeRecognition()
        var timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("nextScene"), userInfo: nil, repeats: false)
    }
    
    func toggleColor(button: UIButton ) {
        if(button.backgroundColor == UIColor.greenColor())
        {
            button.backgroundColor = UIColor.whiteColor()
        }
        else
        {
            button.backgroundColor = UIColor.greenColor()
        }
    }
    
    func nextScene() {
        performSegueWithIdentifier("WordMemorySegue", sender: self)
    }
    
    /**
    *  BEGIN VOICE RECOGNITION METHODS
    */
    
    // on exit of scene stop listening
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        suspendRecognition()
        removeWords()
        removeButtons()
        self.openEarsEventsObserver.delegate = nil
        self.openEarsEventsObserver = nil
        
    }

    // method adds months for voice recognition
    override func addWords() {
        words.append("NEXT")
    }
    
    // method receives data String from voice recognition, indexes into dictionary to receive reference to UIButton
    override func pocketsphinxDidReceiveHypothesis(hypothesis: String, recognitionScore: String, utteranceID: String) {
        
        if(hypothesis == "NEXT") {
            performSegueWithIdentifier("WordMemorySegue", sender: self)
        }
    }
    
    /**
    *  END VOICE RECOGNITION METHODS
    */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

