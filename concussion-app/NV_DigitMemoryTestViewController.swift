//
//  NV_DigitMemoryTestViewController.swift
//  concussion-app
//
//  Created by Alex Rumbaugh on 3/1/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit

class NV_DigitMemoryTestViewController: UIViewController {
    
    var nextButton: UIBarButtonItem!
    var rightbutton: UIButton!
    var wrongbutton: UIButton!
    var instructionsLabel: UILabel!
    
    var header: UILabel!
    var subHeader: UILabel!
    
    var digits: [UILabel] = []
    
    var buttonw: CGFloat = 64
    var buttonh: CGFloat = 64
    
    var score: Int!
    
    
    //var currTrial: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup navigation bar title
        self.navigationItem.title = "Cognitive Assessment"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        nextButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: "nextIteration")
        navigationItem.rightBarButtonItem = nextButton
        nextButton.enabled = false
        
        // set up background color for scene
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        //used to position the digits on the screen
        var numDigits:CGFloat = CGFloat(digitMemoryTestInst.digitsPerTrial[digitMemoryTestInst.currentTrial])
        
        header = UILabel(frame: CGRectMake(0, 0, 280, 45))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 36)
        header.layer.cornerRadius = 5
        header.layer.masksToBounds = true
        header.text = "Concentration"
        self.view.addSubview(header)
        
        //insert first question
        subHeader = UILabel(frame: CGRectMake(0, 0, 280, 45))
        subHeader.center = CGPointMake((screenWidth/2), (screenHeight/4))
        subHeader.textAlignment = NSTextAlignment.Center
        subHeader.font = UIFont(name: subHeader.font.fontName, size: 20)
        subHeader.layer.cornerRadius = 5
        subHeader.layer.masksToBounds = true
        subHeader.text = "Read the following digits"
        self.view.addSubview(subHeader)
        
        //place the right and wrong buttons
        let rwbuttonheight = screenWidth/(2*1.6)
        let ypos : CGFloat = screenHeight - rwbuttonheight
        
        rightbutton = UIButton(frame: CGRectMake(0,ypos,screenWidth/2, rwbuttonheight))
        rightbutton.setTitle("Right", forState: UIControlState.Normal)
        rightbutton.backgroundColor = UIColor(hexString:rightfade)
        rightbutton.addTarget(self, action: "pressRight:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(rightbutton)
        
        wrongbutton = UIButton(frame: CGRectMake(screenWidth/2,ypos,screenWidth/2, rwbuttonheight))
        wrongbutton.setTitle("Wrong", forState: UIControlState.Normal)
        wrongbutton.backgroundColor = UIColor(hexString:wrongfade)
        wrongbutton.addTarget(self, action: "pressWrong:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(wrongbutton)  
        
        setUpDigits()
    }
    
    //displays the digits to the screen
    func setUpDigits() {
        //generate the digits for this trial
        digitMemoryTestInst.generateDigits()
        
        let currTrial = digitMemoryTestInst.currentTrial
        let numDigits: Int = digitMemoryTestInst.digitsPerTrial[currTrial]
        
        let ypad_top : CGFloat = screenHeight * 0.15
        let ypad_bot : CGFloat = screenHeight * 0.15
        let xpad_left : CGFloat = screenWidth * 0.05
        let xpad_right : CGFloat = screenWidth * 0.05
        let cellx : CGFloat = (screenWidth - xpad_left - xpad_right) / CGFloat(numDigits)
        let celly : CGFloat = (screenHeight - ypad_top - ypad_bot)
        
        
        var digitFontSize : CGFloat = 48
        
        //set the labels with digits
        for i in 0...numDigits-1
        {
            var digitLabel = UILabel(frame: CGRectMake(0, 0, buttonw, buttonh))
            digitLabel.backgroundColor = UIColor.whiteColor()
            digitLabel.text = digitMemoryTestInst.chosenDigits[i]
            var xpos: CGFloat = xpad_left + cellx*CGFloat(i) + cellx/2.0
            var ypos: CGFloat = ypad_top + celly/2.0
            digitLabel.frame = CGRectMake(xpos,ypos,buttonw, buttonh)
            digitLabel.center = CGPoint(x: xpos,y: ypos)
            digitLabel.backgroundColor = UIColor(hexString: bgColor)
            digitLabel.font = UIFont(name: digitLabel.font.fontName, size: digitFontSize)
            digitLabel.textAlignment = NSTextAlignment.Center
            digits.append(digitLabel)
            self.view.addSubview(digitLabel)
        }
    }
    
    func nextIteration() {
        let currTrial = digitMemoryTestInst.currentTrial
        updateScore()
        if (currTrial == 3) {
            performSegueWithIdentifier("NVDigit2MonthReversalSegue",sender:self)
        }
        else {
            (digitMemoryTestInst.currentTrial)++
            for digit in digits {
                digit.removeFromSuperview()
            }
            digits.removeAll(keepCapacity: false)
            wrongbutton.backgroundColor = UIColor(hexString:wrongfade)
            rightbutton.backgroundColor = UIColor(hexString:rightfade)
            nextButton.enabled = false
            setUpDigits()
        }
    }
    
    func updateScore() {
        GlobalVariables.sharedInstance.concentrationScore += score
    }
    
    
    func pressWrong(sender : UIButton) {
        score = 0
        nextButton.enabled = true
        rightbutton.backgroundColor = UIColor(hexString:rightfade)
        sender.backgroundColor = UIColor(hexString:wrongbright)
    }
    
    func pressRight(sender : UIButton) {
        score = 1
        nextButton.enabled = true
        wrongbutton.backgroundColor = UIColor(hexString:wrongfade)
        sender.backgroundColor = UIColor(hexString:rightbright)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
