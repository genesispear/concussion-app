import UIKit

class NV_OrientationViewController: UIViewController, UIScrollViewDelegate {
    
    var scrollView: UIScrollView!
    var containerView = UIView()
    
    //border dimensions and measurements
    var ypad_top : CGFloat = 0
    var ypad_bot : CGFloat = 0
    var xpad_left : CGFloat = 0
    var xpad_right : CGFloat = 0
    var headerheight: CGFloat = 0
    var answerheight : CGFloat = 0
    var questionheight : CGFloat = 0
    
    // score of test
    var score = 0
    

    // UILabels and UIButtons collections
    var nextButton: UIButton!
    var questions: [UILabel] = []
    var recorded_answers: [UILabel] = []
    var answerbuttons: [UIButton] = []
    var rightbuttons: [UIButton] = []
    var wrongbuttons: [UIButton] = []
    
    //the buttons the user has selected
    var buttonsPressed: [Int] = [0,0,0,0,0]
    var pressed: [Int] = [0,0,0,0,0]
    var question_strings: [String] = [" What month is it?", " What is the date today?", " What is the day of the week?", " What year is it?", " What time is it right now? (within 1 hour)"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup navigation bar title
        self.navigationItem.title = "Orientation Test"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        var nextButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: "leaveScene")
        navigationItem.rightBarButtonItem = nextButton
        
        var navbounds = self.navigationController?.navigationBar.bounds
        var navheight1 = navbounds?.height
        var navheight : CGFloat = CGFloat(navheight1!) + 20.0
        
        ypad_top = screenHeight * 0.15
        ypad_bot = screenHeight * 0.1
        xpad_left = screenWidth * 0.02
        xpad_right = screenWidth * 0.02
        headerheight = GlobalVariables.sharedInstance.headerheight
        answerheight = GlobalVariables.sharedInstance.answerheight
        questionheight = GlobalVariables.sharedInstance.questionheight
        
        //create the scrollView
        scrollView = UIScrollView()
        self.scrollView.delegate = self
        scrollView.backgroundColor = UIColor(hexString: bgColor)
        scrollView.contentSize = CGSize(width: screenWidth, height: questionheight)
        scrollView.frame = CGRectMake(0,navheight + headerheight,screenWidth, screenHeight)
        self.view.addSubview(scrollView)
    
        //insert instructions
        var q2 = UILabel(frame: CGRectMake(0, navheight, screenWidth, headerheight))
        //q2.center = CGPointMake((screenWidth/2), navheight! + headerheight/2.0)
        q2.textAlignment = NSTextAlignment.Center
        q2.font = UIFont(name: q2.font.fontName, size: 16)
        q2.layer.masksToBounds = true
        q2.text = "Ask the following questions"
        q2.backgroundColor = UIColor(hexString: bgColor)
        self.view.addSubview(q2)
        
        
        //place the question labels
        //set up some stuff
        var row :CGFloat = 0
        var col :CGFloat = 0
        var qcellx : CGFloat = screenWidth
        var qcelly : CGFloat = ((questionheight) / 8.0)
        var qwidth: CGFloat = screenWidth
        var qheight: CGFloat = qcelly / 2.5
        var qFontSize: CGFloat = 16
        for i in 0...question_strings.count-1 {
            var xpos: CGFloat = qcellx/2.0
            var ypos: CGFloat = qcelly*row
            var newlabel = UILabel(frame: CGRectMake(0,ypos,qwidth,qheight))
            
            newlabel.font = UIFont(name: newlabel.font.fontName, size: qFontSize)
            newlabel.text = question_strings[i]
            newlabel.textColor = UIColor.blackColor()
            newlabel.backgroundColor = UIColor(hexString:"#d3ace4")
            newlabel.layer.borderWidth = 1.0
            newlabel.numberOfLines = 0;
            newlabel.textAlignment = NSTextAlignment.Center;
            questions.append(newlabel)
            scrollView.addSubview(questions[i])
            
            var rightB = UIButton(frame: CGRectMake(0,ypos+qheight,qwidth/2.0,1.5*qheight))
            rightB.setTitle("Right", forState: UIControlState.Normal)
            rightB.backgroundColor = UIColor(hexString:rightfade)
            rightB.tag = i
            rightB.addTarget(self, action: "pressRight:", forControlEvents: UIControlEvents.TouchUpInside)
            rightbuttons.append(rightB)
            scrollView.addSubview(rightbuttons[i])
            
            var wrongB = UIButton(frame: CGRectMake(screenWidth/2.0,ypos+qheight,qwidth/2.0,1.5*qheight))
            wrongB.setTitle("Wrong", forState: UIControlState.Normal)
            wrongB.backgroundColor = UIColor(hexString:wrongfade)
            wrongB.tag = i
            wrongB.addTarget(self, action: "pressWrong:", forControlEvents: UIControlEvents.TouchUpInside)
            wrongbuttons.append(wrongB)
            scrollView.addSubview(wrongbuttons[i])

            row++
        }
    }
    
    func pressWrong(sender: UIButton) {
        rightbuttons[sender.tag].backgroundColor = UIColor(hexString:rightfade)
        sender.backgroundColor = UIColor(hexString:wrongbright)
        pressed[sender.tag] = 0
    }
    
    func pressRight(sender: UIButton) {
        if(pressed[sender.tag] != 1) {
            wrongbuttons[sender.tag].backgroundColor = UIColor(hexString:wrongfade)
            sender.backgroundColor = UIColor(hexString:rightbright)
            pressed[sender.tag] = 1
        }
    }
    
    func hideAnswerButtons() {
        for button in answerbuttons {
            button.setTitle("", forState: UIControlState.Normal)
            button.removeTarget(self, action: "pressAnswer:", forControlEvents: UIControlEvents.TouchUpInside)
            button.center = CGPoint(x:0,y:0)
        }
    }
    
    func storeAnswers() {
        var i = 0
        for ans in recorded_answers {
            let anstext = ans.text!
            GlobalVariables.sharedInstance.maddocks_ans[i] = anstext
            i++
        }
    }
    
    // function is called right before scene switches to calculate score
    func calculateScore() {
        for num in pressed {
            GlobalVariables.sharedInstance.orientationScore += num
        }
    }
    
    // function is called when scene transition occurs
    func leaveScene() {
        calculateScore()
        performSegueWithIdentifier("Orientation2WordMemory", sender:self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
