//
//  Swift-Bridging-Header.h
//  concussion-app
//
//  Created by Michael Mathew on 4/9/15.
//  Copyright (c) 2015 Michael Mathew. All rights reserved.
//

#ifndef concussion_app_Swift_Bridging_Header_h
#define concussion_app_Swift_Bridging_Header_h

#import <OpenEars/OELanguageModelGenerator.h>
//#import <RejectoDemo/OELanguageModelGenerator+Rejecto.h>
#import <OpenEars/OEAcousticModel.h>
#import <OpenEars/OEPocketsphinxController.h>
#import <OpenEars/OEEventsObserver.h>


#endif
