//
//  BalanceTestViewController.swift
//  concussion-app
//
//  Created by Michael Mathew on 5/9/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//
import UIKit
import CoreMotion

class maddocksScoreViewController: OpenEarsViewController {
    
    var testTitle: UILabel!
    var numOfErrors: UILabel!
    var timeLabel: UILabel!
    var numOfErrorsHeader: UILabel!
    var timeLabelHeader: UILabel!
    var balanceTestButton: UIButton!
    
    
    
    // variables for test
    var errors = 0
    var firstGyroIteration = false
    var initialStateGyroValue = 0.0
    var counter = 0
    var timeOfLastErr = 0
    var testDone = false
    
    
    // used for placement of UILabels
    var xloc = screenWidth / 2.0;
    var yloc = screenHeight / 7.0 + 5.0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*******FOR TESTING PURPOSES SKIP BALANCE TEST AND GO STRAIGHT TO SCORE SCREEN **********/
        //performSegueWithIdentifier("ScoreScreenSegue", sender: self)
        /******DELETE THIS AFTERWARDS*******/
        
        
        //setup navigation bar title
        self.navigationItem.title = "Maddocks Results"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        // sets up the background color
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        resultsOfMaddocksTestView()
    }
    
    func gotoHome(){
        performSegueWithIdentifier("toHome", sender: self)
    }
    
    func gotoScat(){
        println("to scat!")
        performSegueWithIdentifier("toScat", sender: self)
    }
    
    // used for case when user completes JUST balance test so they can see score
    func resultsOfMaddocksTestView () {
        // set up the labels for the score screen
        var testTitle = UILabel(frame : CGRectMake(screenWidth,100,0,0))
        var numOfErrorsHeader = UILabel(frame: CGRectMake(screenWidth,100,0,0))
        var numOfErrors = UILabel(frame:CGRectMake(screenWidth,100,20,20))
        
        testTitle.text = "Maddocks Test Results"
        numOfErrorsHeader.text = "Total number of Errors"
        numOfErrors.text = String(GlobalVariables.sharedInstance.maddocksScore)
        numOfErrors.font = numOfErrors.font.fontWithSize(20)

        
        testTitle.sizeToFit()
        numOfErrorsHeader.sizeToFit()
        //numOfErrors.sizeToFit()
        
        testTitle.center = CGPoint(x: xloc, y:yloc)
        numOfErrorsHeader.center = CGPoint(x: xloc, y:yloc*2)
        numOfErrors.center = CGPoint(x: xloc, y:yloc*3)
        
        // enable return home button
        var balanceTestButton = UIButton(frame: CGRectMake(screenWidth/8,100,screenWidth-8,75))
        balanceTestButton.hidden = false
        balanceTestButton.setTitle("Return Home", forState: UIControlState.Normal)
        //balanceTestButton.sizeToFit()
        balanceTestButton.layer.cornerRadius = 5
        balanceTestButton.layer.borderWidth = 1
        balanceTestButton.layer.borderColor = UIColor.whiteColor().CGColor
        balanceTestButton.layer.backgroundColor = UIColor(hexString: "#a85ac9")?.CGColor
        balanceTestButton.center = CGPoint(x: xloc, y:yloc*4)
        balanceTestButton.addTarget(self, action: "gotoHome", forControlEvents: UIControlEvents.TouchUpInside)
        
        // enable return home button
        var scatTestButton = UIButton(frame: CGRectMake(screenWidth/8,100,screenWidth-8,75))
        scatTestButton.hidden = false
        scatTestButton.setTitle("Begin SCAT", forState: UIControlState.Normal)
        //scatTestButton.sizeToFit()
        scatTestButton.layer.cornerRadius = 5
        scatTestButton.layer.borderWidth = 1
        scatTestButton.layer.borderColor = UIColor.whiteColor().CGColor
        scatTestButton.layer.backgroundColor = UIColor(hexString: "#a85ac9")?.CGColor
        scatTestButton.center = CGPoint(x: xloc, y:yloc*5)
        scatTestButton.addTarget(self, action: "gotoScat", forControlEvents: UIControlEvents.TouchUpInside)
        
        self.view.addSubview(testTitle)
        self.view.addSubview(numOfErrorsHeader)
        self.view.addSubview(numOfErrors)
        self.view.addSubview(balanceTestButton)
        self.view.addSubview(scatTestButton)
    }

    // on scene change perform clean up actions
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

