//
//  Orientation.swift
//  concussion-app
//
//  Created by Michael Banzon on 3/1/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit


//obtain date
let date = NSDate()
let calendar = NSCalendar.currentCalendar()
let components = calendar.components(.CalendarUnitMonth | .CalendarUnitWeekday | .CalendarUnitYear | .CalendarUnitHour | .CalendarUnitDay, fromDate: date)
let month = components.month
let weekday = components.weekday
let currYear = components.year
var currHour = components.hour
let day = components.day


class MonthOrientationController:OpenEarsViewController {
    
    @IBOutlet var nextButton: UIButton!
//    @IBOutlet var micStatus: UILabel!
    
    // variable used to store the current month that is selected
    var selectedMonth: String = ""
    
    @IBOutlet var buttonarray: [UIButton] = []
    var months: [String] = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE",
        "JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set Up the Nav Bar
        self.navigationItem.title = "Orientation Assessment"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        nextButton.enabled = false;
        
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        //insert Header "Date and Time"
        var header = UILabel(frame: CGRectMake(0, 0, screenWidth, 60))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 40)
        header.layer.cornerRadius = 0
        header.layer.masksToBounds = true
        header.text = "Date and Time"
        //header.backgroundColor = UIColor(hexString:"#e4cdee")
        
        self.view.addSubview(header)
        
        //insert first question
        var q1 = UILabel(frame: CGRectMake(0, 0, screenWidth, 50))
        q1.center = CGPointMake((screenWidth/2), (screenHeight/4))
        q1.textAlignment = NSTextAlignment.Center
        
        q1.font = UIFont(name: q1.font.fontName, size: 20)
        q1.layer.cornerRadius = 0
        q1.layer.masksToBounds = true
        q1.text = "What month is it?"
        //q1.backgroundColor = UIColor(hexString:"#e4cdee")
        
        self.view.addSubview(q1)
        
        // mic status ******* TODO === MAYBE TAKE THIS OUT????? ***********
//        micStatus = UILabel(frame: CGRectMake(0, 0, 65.0, 60.0))
//        micStatus.center = CGPointMake(screenWidth/2.0, screenHeight*(7/8))
//        micStatus.clipsToBounds = true
//        micStatus.backgroundColor = UIColor.greenColor()
//        micStatus.layer.cornerRadius = 100/4.0
//        micStatus.text = "Mic Off"
//        micStatus.textAlignment = NSTextAlignment.Center
//        
//        self.view.addSubview(micStatus)
        
        self.buttonLayout()
        
        // Text-To-Speech command for test
        voiceCommand("Please indicate the current month")
    }
    
    override func viewDidAppear(animated: Bool) {
        addWords()
        changeLanguageModel()
    }
    
    // event is fired AT THE END OF THE VOICE DICTATION
    override func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didFinishSpeechUtterance utterance: AVSpeechUtterance!) {
        resumeRecognition()
    }
    
    func buttonLayout() {
        var ypad : CGFloat = 0.3 * screenHeight;
        var xpad : CGFloat = 0.10 * screenWidth;
        var xcell : CGFloat;
        var ycell : CGFloat;
        xcell = screenWidth
        xcell = (screenWidth - 2.0*xpad) / 2.0
        ycell = (screenHeight - 1.3*ypad) / 6.0
        var i = 0
        var row : CGFloat = 0.0;
        var col : CGFloat = 0.0;
        for button in buttonarray {
            
            buttonArray[months[i]] = button
            button.setTitle(months[i], forState: UIControlState.Normal)
            button.frame = CGRectMake(screenWidth*(0.15+row), screenHeight*(1/6+col), 108, 36)
            var xpos = xpad + xcell * col + xcell / 2.0
            var ypos = ypad + ycell * row + ycell / 2.0
            button.center = CGPoint(x: xpos, y: ypos)
            button.layer.borderColor = UIColor.blackColor().CGColor
            button.layer.borderWidth = 1.0
            button.layer.cornerRadius = 3.0
            button.clipsToBounds = true
            button.backgroundColor = UIColor(hexString: buttonColor)
            button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
            button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            //button.layer.cornerRadius = 100/2.0
            
            if(i % 2 == 0) {
                col++;
            }
            else {
                row++;
                col = 0;
            }
            
            i++;
        }
    }
    
    // carpet elbow penny baby paper
    //method is called when user clicks on a label
    func buttonAction(sender:UIButton!)
    {
        nextButton.enabled = true;
        changeButtonColor(sender)
        
        for button in buttonarray {
            if button != sender {
                button.backgroundColor = UIColor(hexString: buttonColor)
            }
        }
        
        // assign the currently selected label to the selectedMonth
        selectedMonth = sender.titleLabel!.text!
    }
    
    func checkAnswer() {
        //if they got the month correct
        if selectedMonth == months[month - 1] {
            println("correct month")
            GlobalVariables.sharedInstance.orientationScore++
        }
    }
    
    // method to change button color once selected or deselected
    func changeButtonColor(button: UIButton){
        if(button.backgroundColor == UIColor(hexString:buttonColor))
        {
            button.backgroundColor = UIColor(hexString: buttonPressed)
        }
    }
    
    /**
    *  BEGIN VOICE RECOGNITION METHODS
    */
    
    // on exit of scene stop listening
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        suspendRecognition()
        checkAnswer()
        removeWords()
        removeButtons()
        self.openEarsEventsObserver.delegate = nil
        self.openEarsEventsObserver = nil
        
    }
    
    // method adds months for voice recognition
    override func addWords() {
        words.append("JANUARY")
        words.append("FEBRUARY")
        words.append("MARCH")
        words.append("APRIL")
        words.append("MAY")
        words.append("JUNE")
        words.append("JULY")
        words.append("AUGUST")
        words.append("SEPTEMBER")
        words.append("OCTOBER")
        words.append("NOVEMBER")
        words.append("DECEMBER")
        words.append("NEXT")
    }
    
    // method receives data String from voice recognition, indexes into dictionary to receive reference to UIButton
    override func pocketsphinxDidReceiveHypothesis(hypothesis: String, recognitionScore: String, utteranceID: String) {
        if(hypothesis == "NEXT") {
            performSegueWithIdentifier("MonthTestSegue", sender: self)
        }
            
        else if( find(months,hypothesis) != nil){
            let selBtnArray = hypothesis.componentsSeparatedByString(" ")
            let selBtnString = (selBtnArray[0] as NSString)
            let selBtnRef = buttonArray[selBtnString as String]!
            
            buttonAction(selBtnRef)
        }
    }
    
    /*********************** TODO ==== Maybe get rid of this mic status stuff?? ***************/
//    override func pocketsphinxDidResumeRecognition() {
//        micStatus.backgroundColor = UIColor.redColor()
//        micStatus.text = "Mic On"
//    }
//    
//    override func pocketsphinxDidSuspendRecognition() {
//        micStatus.backgroundColor = UIColor.greenColor()
//        micStatus.text = " Mic Off"
//    }
    
    
    
    /**
    *  END VOICE RECOGNITION METHODS
    */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}