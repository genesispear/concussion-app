//
//  global.swift
//  concussion-app
//
//  Created by Alex Rumbaugh on 2/21/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit


// enable UIColor extension globally
extension UIColor {
    convenience init(r: Int, g:Int , b:Int , a: Int) {
        self.init(red: CGFloat(r)/255, green: CGFloat(g)/255, blue: CGFloat(b)/255, alpha: CGFloat(a)/255)
    }
}



//obtain screen size
let screenSize: CGRect = UIScreen.mainScreen().bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height


//colors
let menuColor = "#a85ac9"
let bgColor = "#eae8ea"
let buttonColor = "#f6eef9"
let buttonPressed = "#7bc95a"

//button colors
var wrongfade = "#e9bdca"
var wrongbright = "#c95a7b"

var rightfade = "#cae9bd"
var rightbright = "#7bc95a"

//indicate which balance test to test
var balanceTestIndex = 0;

//Model Classes for tests
var wordMemoryTestInst = wordMemoryClass()
var digitMemoryTestInst = digitMemoryClass()

class GlobalVariables {
    //Singleton specific implementation
    class var sharedInstance: GlobalVariables {
        struct Static {
            static var instance: GlobalVariables?
            static var token: dispatch_once_t = 0
        }
            
        dispatch_once(&Static.token) {
            Static.instance = GlobalVariables()
        }
            
        return Static.instance!
    }
    
    // flag used to determine if the app is loading FOR THE FIRST TIME
    var initialStartup = true
    
    // test state flag
    // 1 for normal full test, self administered with maddocks
    // 2 for just balance test
    // 3 for just maddocks
    // 4 for administered version of test
    var testStateFlag = 0

    // delayed recall flag is used to trigger event of delayed recall
    var delayedRecallFlag: Int = 0
    
    // used when running an independent Balance test
    var indepBalanceFlag: Int = 0
    
    //maddocks score answers
    var maddocks_ans: [String] = ["","","","",""]
    var maddocksScore : Int = 0
    
    // symtom test score variables
    var symTotal: Int = 0
    var symSeverityScore: Int = 0
    
    // orientation score
    var orientationScore: Int = 0
    
    // concentration score
    var concentrationScore: Int = 0
    
    // immediate memory score
    var immediateMemoryScore: Int = 0
    
    // balance test score
    var balanceTestScore: Int = 0
    
    //delayed recall score
    var delayedRecallScore: Int = 0

    
    // memory test score 
    var wordMemoryTestScore: [Int] = [0,0,0]
    var digitMemoryTestScore: [Int] =  [0,0,0]
    
    var headerheight = 0.05 * screenHeight
    var answerheight = screenHeight * 0.14
    var questionheight = 1.6*screenHeight
    
    func getSacTotalScore() -> Int {
        let sum = concentrationScore + immediateMemoryScore + orientationScore + delayedRecallScore

        return sum
    }
    
    func resetAllScoreVars() {
        symTotal = 0
        symSeverityScore = 0
        orientationScore = 0
        immediateMemoryScore = 0
        balanceTestScore = 0
        delayedRecallScore = 0
        wordMemoryTestInst.currentIteration = 0
        digitMemoryTestInst.currentTrial = 0
        delayedRecallFlag = 0
        testStateFlag = 0
        wordMemoryTestInst = wordMemoryClass()
        digitMemoryTestInst = digitMemoryClass()
    }
}