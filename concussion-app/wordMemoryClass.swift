//
//  wordMemory.swift
//  concussion-app
//
//  Created by Alex Rumbaugh on 2/21/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import Foundation

class wordMemoryClass {
    
    var chosenWords :[String] = []
    var displayedWords :[String] = []
    var numChosen = 5
    var numDisplayed = 15
    var numSelected = 0
    var currentIteration = 0
    
    var availableWords: [String] = ["Apple","Bananas","Elbow","Carpet","Saddle","Bubble","Candle",
        "Paper","Sugar","Sandwich","Wagon","Monkey","Perfume","Sunset","Iron","Finger","Penny",
        "Blanket","Lemon","Insect"]

    var selectedWords = Dictionary<String, Int>()
    
    func generateWords() {
        var mutableSet = NSMutableSet()
        

        while mutableSet.count < numChosen {
            let randomNumber = arc4random_uniform(UInt32(availableWords.count))
            let randomName = availableWords[Int(randomNumber)]
            mutableSet.addObject(randomName)
        }
        
        var i = 0
        for word in mutableSet {
            chosenWords.insert(word as! String, atIndex: i)
            i++
        }
    }
}