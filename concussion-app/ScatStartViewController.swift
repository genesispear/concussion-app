//
//  BalanceTestViewController.swift
//  concussion-app
//
//  Created by Michael Mathew on 5/9/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//
import UIKit
import CoreMotion

class scatStartViewController: OpenEarsViewController {
    
    var testTitle: UILabel!
    var numOfErrors: UILabel!
    var timeLabel: UILabel!
    var numOfErrorsHeader: UILabel!
    var timeLabelHeader: UILabel!
    var balanceTestButton: UIButton!
    
    
    
    // variables for test
    var errors = 0
    var firstGyroIteration = false
    var initialStateGyroValue = 0.0
    var counter = 0
    var timeOfLastErr = 0
    var testDone = false
    
    
    // used for placement of UILabels
    var xloc = screenWidth / 2.0;
    var yloc = screenHeight / 7.0 + 5.0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //setup navigation bar title
        self.navigationItem.title = "SCAT 3 Menu"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        // sets up the background color
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        resultsOfMaddocksTestView()
    }
    
    func gotoAdminScat(){
        performSegueWithIdentifier("toAdminScat", sender: self)
        //performSegueWithIdentifier("toDigit", sender: self)
    }
    
    func gotoSelfScat(){
        performSegueWithIdentifier("toSelfScat", sender: self)
    }
    
    // used for case when user completes JUST balance test so they can see score
    func resultsOfMaddocksTestView () {
        // set up the labels for the score screen
        var testTitle = UILabel(frame : CGRectMake(screenWidth,100,0,0))
        var numOfErrorsHeader = UILabel(frame: CGRectMake(screenWidth,100,0,0))
        
        testTitle.text = "SCAT 3 Test"
        testTitle.font = testTitle.font.fontWithSize(36.0)
        numOfErrorsHeader.text = "Select an option"
        numOfErrorsHeader.font = numOfErrorsHeader.font.fontWithSize(25.0)

        
        testTitle.sizeToFit()
        numOfErrorsHeader.sizeToFit()
        
        testTitle.center = CGPoint(x: xloc, y:yloc)
        numOfErrorsHeader.center = CGPoint(x: xloc, y:yloc*2)
        
        // enable return home button
        var balanceTestButton = UIButton(frame: CGRectMake(screenWidth/8,100,screenWidth-4,100))
        balanceTestButton.hidden = false
        balanceTestButton.setTitle("Begin Self-Administered", forState: UIControlState.Normal)
        //balanceTestButton.sizeToFit()
        balanceTestButton.layer.cornerRadius = 5
        balanceTestButton.layer.borderWidth = 1
        balanceTestButton.layer.borderColor = UIColor.whiteColor().CGColor
        balanceTestButton.layer.backgroundColor = UIColor(hexString: "#a85ac9")?.CGColor
        balanceTestButton.center = CGPoint(x: xloc, y:yloc*3.0)
        balanceTestButton.addTarget(self, action: "gotoSelfScat", forControlEvents: UIControlEvents.TouchUpInside)
        
        // enable return home button
        
        /*
        scatTestLabel = UIButton(frame: CGRectMake(0,yloc,buttonw,buttonh))

        scatTestLabel.layer.borderColor = UIColor.whiteColor().CGColor
        scatTestLabel.layer.backgroundColor = UIColor(hexString: "#a85ac9")?.CGColor
        scatTestLabel.setTitle("Start SCAT3", forState: UIControlState.Normal)
        scatTestLabel.addTarget(self, action: "toSCAT", forControlEvents: UIControlEvents.TouchUpInside)
        */
        
        var scatTestButton = UIButton(frame: CGRectMake(screenWidth/8,100,screenWidth-4,100))
        scatTestButton.hidden = false
        scatTestButton.setTitle("Begin Administered", forState: UIControlState.Normal)
        //scatTestButton.sizeToFit()
        scatTestButton.layer.cornerRadius = 5
        scatTestButton.layer.borderWidth = 1
        scatTestButton.layer.borderColor = UIColor.whiteColor().CGColor
        scatTestButton.layer.backgroundColor = UIColor(hexString: "#a85ac9")?.CGColor
        scatTestButton.center = CGPoint(x: xloc, y:yloc*4.5)
        scatTestButton.addTarget(self, action: "gotoAdminScat", forControlEvents: UIControlEvents.TouchUpInside)
        
        self.view.addSubview(testTitle)
        self.view.addSubview(numOfErrorsHeader)
        self.view.addSubview(balanceTestButton)
        self.view.addSubview(scatTestButton)
    }
    
    // on scene change perform clean up actions
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

