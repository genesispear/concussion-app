//
//  NV_SymEvalController.swift
//  concussion-app
//
//  Created by Nathaniel Ho on 2/21/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit
import AVFoundation

class NV_SymEvalController: UIViewController {
    var buttonarray: [UIButton] = []
    var nextButton: UIBarButtonItem!
    var questionLabel: UILabel!
    var symTotal: Int = 0 // Total number of symptoms (Maximum possible 22)
    var symSeverityScore: Int = 0 // Symptom severity score (Maximum possible 132)
    var currentElm: Int = 0
    var questionVals: [Int] = Array(count:22, repeatedValue: 0)
    
    // total 22 items
    var questionItems: [String] = [
        "Headache",
        "\"Pressure in head\"",
        "Neck Pain",
        "Nausea or vomiting",
        "Dizziness",
        "Blurred vision",
        "Balance problems",
        "Sensitivity to light",
        "Sensitivity to noise",
        "Feeling slowed down",
        "Feeling like \"in a fog\"",
        "\"Don't feel right\"",
        "Difficulty concentrating",
        "Difficulty remembering",
        "Fatigue or low energy",
        "Confusion",
        "Drowsiness",
        "Trouble falling asleep",
        "More emotional",
        "Irritability",
        "Sadness",
        "Nervous or Anxious"]
    //index of button currently pressed
    var pressed = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.title = "Symptom Eval"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        self.view.backgroundColor = UIColor(hexString:bgColor)
        nextButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: "buttonAction:")
        navigationItem.rightBarButtonItem = nextButton
        nextButton.enabled = false
        
        var header = UILabel(frame: CGRectMake(0, 0, 280, 45))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 26)
        header.layer.cornerRadius = 5
        header.layer.masksToBounds = true
        header.text = "How are you feeling?"
        self.view.addSubview(header)
        
        questionLabel = UILabel(frame: CGRectMake(0, 0, 280, 45))
        questionLabel.center = CGPointMake((screenWidth/2), (screenHeight/4))
        questionLabel.textAlignment = NSTextAlignment.Center
        questionLabel.font = UIFont(name: questionLabel.font.fontName, size: 20)
        questionLabel.layer.cornerRadius = 5
        questionLabel.layer.masksToBounds = true
        questionLabel.text = questionItems[0]
        self.view.addSubview(questionLabel)
        
        self.buttonLayout()
    }
    
    func buttonLayout() {
        
        var i = 0
        var row : CGFloat = 0.0;
        var col : CGFloat = 0.0;
        var xpad : CGFloat = 0.05 * screenWidth
        var xcell : CGFloat = (screenWidth - 2 * xpad) / 7.0
        
        for i in 0...6 {
            var button = UIButton(frame: CGRectMake(0,0, xcell-4, 1.6*(xcell-4)))
            
            button.setTitle("\(i)", forState: UIControlState.Normal)
            button.frame = CGRectMake(0, 0, xcell-4, 1.6*(xcell-4))
            
            let xpos = xpad + row*xcell + xcell / 2.0
            let ypos = 0.5 * screenHeight
            
            button.center = CGPoint(x:xpos,y:ypos)
            
            button.layer.borderColor = UIColor.blackColor().CGColor
            button.layer.borderWidth = 0.5
            button.clipsToBounds = true
            button.backgroundColor = UIColor(hexString: buttonColor)
            button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
            button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            button.titleLabel!.font = UIFont(name: button.titleLabel!.font.fontName, size: 24)
            button.tag = i
            //button.enabled = false

            buttonarray.append(button)
            self.view.addSubview(button)
            row++
        }
        nextButton.enabled = false
        println(buttonArray.count)
    }
    
    func buttonAction(sender:UIButton!) {
        // move increment/decrement index in questionItems array
        // depending on which button was pressed
        if(sender == nextButton) {
            // currentElm == index of last set of questions
            // clicking "Next" should push new screen instead of reloading data
            if(currentElm == questionItems.count - 1) {
                // calculate sums
                symTotal = questionVals.reduce(0) {
                    if($1 > 0) {
                        return $0 + 1
                    }
                    return $0
                }
                symSeverityScore = questionVals.reduce(0) {
                    return $0 + $1
                }
                // send data to next screen, push next screen
                GlobalVariables.sharedInstance.symTotal = self.symTotal
                GlobalVariables.sharedInstance.symSeverityScore = self.symSeverityScore
                performSegueWithIdentifier("nextSymEval", sender: nil)
                return
            }
            questionLabel.text = questionItems[++currentElm]
            nextButton.enabled = false
            pressed = -1
            for button in buttonarray {
                button.backgroundColor = UIColor(hexString: buttonColor)
            }
        }
        else {
            let tag = sender.tag
            if( pressed != tag)
            {
                if(pressed != -1)
                {
                    buttonarray[pressed].backgroundColor = UIColor(hexString: buttonColor)
                }
                sender.backgroundColor = UIColor(hexString: buttonPressed)
                pressed = sender.tag
                questionVals[currentElm] = sender.titleLabel!.text!.toInt()!
                nextButton.enabled = true
                }
        }
    }
    
    // changes the color of the button
    func changeButtonColor(button: UIButton) {
        if(button.backgroundColor == UIColor(hexString: buttonColor)) {
            button.backgroundColor = UIColor(hexString: buttonPressed)
        }
        else {
            button.backgroundColor = UIColor(hexString: buttonColor)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

