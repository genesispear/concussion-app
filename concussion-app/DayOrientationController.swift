//
//  Orientation.swift
//  concussion-app
//
//  Created by Michael Banzon on 3/1/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit


class DayOrientationController:OpenEarsViewController {
    
    @IBOutlet var nextButton: UIButton!
    
    // variable used to store the currently selected Day
    var selectedDay: String = ""
    
    @IBOutlet var buttonarray: [UIButton] = []
//    var micStatus: UILabel!
    var days: [String] = ["SUNDAY","MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY","SATURDAY"];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Orientation Assessment"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        //self.navigationItem.setHidesBackButton(true, animated: false)
        
        
        nextButton.enabled = false;
        
        //insert Header "Date and Time"
        var header = UILabel(frame: CGRectMake(0, 0, 280, 45))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 40)
        header.layer.cornerRadius = 5
        header.layer.masksToBounds = true
        header.text = "Date and Time"
        
        self.view.addSubview(header)
        
        //insert first question
        var q2 = UILabel(frame: CGRectMake(0, 0, 280, 45))
        q2.center = CGPointMake((screenWidth/2), (screenHeight/4))
        q2.textAlignment = NSTextAlignment.Center
        q2.font = UIFont(name: q2.font.fontName, size: 20)
        q2.layer.cornerRadius = 5
        q2.layer.masksToBounds = true
        q2.text = "What is the day of the week?"
        
        self.view.addSubview(q2)
        
        
        /************TODO ==== Maybe get rid of this???? ****************/
//        micStatus = UILabel(frame: CGRectMake(0, 0, 65.0, 60.0))
//        micStatus.center = CGPointMake(screenWidth/2.0, screenHeight*(7/8))
//        micStatus.clipsToBounds = true
//        micStatus.backgroundColor = UIColor.greenColor()
//        micStatus.layer.cornerRadius = 100/4.0
//        micStatus.text = "Mic Off"
//        micStatus.textAlignment = NSTextAlignment.Center
//        
//        self.view.addSubview(micStatus)
        
        self.buttonLayout()
        
        // Text-To-Speech command for test
        voiceCommand("Please indicate the current day")
    }
    
    override func viewDidAppear(animated: Bool) {
        addWords()
        changeLanguageModel()
    }
    
    // event is fired AT THE END OF THE VOICE DICTATION
    override func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didFinishSpeechUtterance utterance: AVSpeechUtterance!) {
        resumeRecognition()
    }
    
    func buttonLayout() {
        var ypad : CGFloat = 0.3 * screenHeight;
        var ycell : CGFloat;
        ycell = (screenHeight - 1.3*ypad) / 7.0
        var i = 0
        var row : CGFloat = 0.0;
        for button in buttonarray {
            buttonArray[days[i]] = button
            button.setTitle(days[i], forState: UIControlState.Normal)
            button.frame = CGRectMake(screenWidth*(0.09 + row), screenHeight*(0.40), 108, 36)
            var ypos = ypad + ycell * row + ycell / 2.0
            button.center = CGPoint(x: screenWidth/2.0, y: ypos)
            button.backgroundColor = UIColor(hexString: buttonColor)
            button.layer.borderWidth = 1.0
            button.layer.cornerRadius = 3.0
            button.clipsToBounds = true
            button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
            button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            row++;
            i++;
        }
    }
    
    // action once button is selected
    func buttonAction(sender:UIButton!) {
        nextButton.enabled = true;
        changeButtonColor(sender)
        
        for button in buttonarray {
            if button != sender {
                button.backgroundColor = UIColor(hexString: buttonColor)
            }
        }
        
        selectedDay = sender.titleLabel!.text!
    }
    
    // checks if the currently selected month is correct
    func checkAnswer() {
        if selectedDay == days[weekday - 1] {
            println("correct weekday")
            GlobalVariables.sharedInstance.orientationScore++
        }
    }
    
    //function to change button color
    func changeButtonColor(button: UIButton) {
        if(button.backgroundColor == UIColor(hexString: buttonColor)){
            button.backgroundColor = UIColor(hexString: buttonPressed)
        }
    }
    
    
    
    
    
    
    
    /**
    *  BEGIN VOICE RECOGNITION METHODS
    */
    
    // method receives data String from voice recognition, indexes into dictionary to receive reference to UIButton
    override func pocketsphinxDidReceiveHypothesis(hypothesis: String, recognitionScore: String, utteranceID: String) {
        if(hypothesis == "NEXT") {
            performSegueWithIdentifier("DayTestSegue", sender: self)
        }
            
        else {
            let selBtnArray = hypothesis.componentsSeparatedByString(" ")
            let selBtnString = (selBtnArray[0] as NSString)
            let selBtnRef = buttonArray[selBtnString as String]!
            
            buttonAction(selBtnRef)
        }
    }
    
    // on exit of scene suspend listening
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        suspendRecognition()
        checkAnswer()
        removeWords()
        removeButtons()
        self.openEarsEventsObserver.delegate = nil
        self.openEarsEventsObserver = nil
    }
    
    /*************TODO Maybe get rid of Mic status???? ******************/
//    override func pocketsphinxDidResumeRecognition() {
//        micStatus.backgroundColor = UIColor.redColor()
//        micStatus.text = "Mic On"
//    }
//    
//    override func pocketsphinxDidSuspendRecognition() {
//        micStatus.backgroundColor = UIColor.greenColor()
//        micStatus.text = " Mic Off"
//    }
    
    override func addWords() {
        words.append("SUNDAY")
        words.append("MONDAY")
        words.append("TUESDAY")
        words.append("WEDNESDAY")
        words.append("THURSDAY")
        words.append("FRIDAY")
        words.append("SATURDAY")
        words.append("NEXT")
    }
    
    /**
    *  END VOICE RECOGNITION METHODS
    */
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}