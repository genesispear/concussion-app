//
//  NV_WordMemoryViewController.swift
//  concussion-app
//
//  Created by Michael Mathew on 5/30/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit

class NV_WordMemoryViewController: UIViewController {
    
    var header: UILabel!
    var subHeader: UILabel!
    
    // array of the buttons on the screen
    var wordButtons: [UIButton] = []
    
    // array of buttons state
    var pressed: [Int] = [0,0,0,0,0]
    
    // index of the current trial
    var wordMemoryTestTrialIndex: Int = 1
    
    // setup global positioning variables
    let ypad_top : CGFloat = screenHeight * 0.30
    let ypad_bot : CGFloat = screenHeight * 0.25
    let xpad_left : CGFloat = screenWidth * 0.05
    let xpad_right : CGFloat = screenWidth * 0.05
    let buttonw :CGFloat = 250
    let buttonh :CGFloat = 75
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup navigation bar title
        self.navigationItem.title = "Cognitive Assessment"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        var nextButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: "leaveScene")
        navigationItem.rightBarButtonItem = nextButton
        
        // setup view
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        // only generate words the first time
        if (wordMemoryTestInst.currentIteration == 0) {
            wordMemoryTestInst.generateWords()
            wordMemoryTestInst.currentIteration++
        }
        
        // Insert header for word memory test
        header = UILabel(frame: CGRectMake(0, 0, screenWidth, 45))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 36)
        header.layer.cornerRadius = 5
        header.layer.masksToBounds = true
        header.text = (GlobalVariables.sharedInstance.testStateFlag == 1 && GlobalVariables.sharedInstance.delayedRecallFlag == 1) ? "Delayed Recall" : "Immediate Memory"
        self.view.addSubview(header)
        
        // insert sub header
        subHeader = UILabel(frame: CGRectMake(0, 0, 280, 45))
        subHeader.center = CGPointMake((screenWidth/2), (screenHeight/4))
        subHeader.textAlignment = NSTextAlignment.Center
        subHeader.font = UIFont(name: subHeader.font.fontName, size: 20)
        subHeader.layer.cornerRadius = 5
        subHeader.layer.masksToBounds = true
        subHeader.text = "Remember the following words"
        self.view.addSubview(subHeader)

        // positioning variables for labels
        var rownum : CGFloat = 0
        var colnum : CGFloat = 0
        var xpos: CGFloat = 0
        var ypos: CGFloat = 0
        var ycell = (screenHeight - 1.7 * ypad_top) / 5.0
        
        // create and display the buttons with words to the view
        var row :CGFloat = 0
        for (var i = 0; i < wordMemoryTestInst.chosenWords.count; i++) {
            var wordButton = UIButton(frame: CGRectMake(0, 0, 120, 32))
            wordButton.center = CGPointMake(screenWidth / 2.0, ypad_top + row * ycell + ycell / 2.0)
            wordButton.setTitle(wordMemoryTestInst.chosenWords[i], forState: UIControlState.Normal)
            wordButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            wordButton.backgroundColor = UIColor(hexString: buttonColor)
            wordButton.tag = i
            wordButton.addTarget(self, action: "pressRight:", forControlEvents: UIControlEvents.TouchUpInside)
            wordButtons.append(wordButton)
            self.view.addSubview(wordButton)
            row++
        }
    }
    
    // function is used for when user presses the right and wrong button to toggle color
    func pressRight(sender : UIButton) {
        // set the background to white && change the state
        if(pressed[sender.tag] == 1) {
            sender.backgroundColor = UIColor(hexString:buttonColor)
            pressed[sender.tag] = 0
        }
        // toggle button color to green && change the state
        else {
            sender.backgroundColor = UIColor(hexString: rightbright)
            pressed[sender.tag] = 1
        }
    }
    
    // function is called to calculate score for screen, and store in global variable
    func calculateScore() {
        for num in pressed {
            // delayed recall score
            if (GlobalVariables.sharedInstance.delayedRecallFlag == 1 && GlobalVariables.sharedInstance.testStateFlag == 1) {
                GlobalVariables.sharedInstance.delayedRecallScore += num
            }
            // word memory score
            else {
                GlobalVariables.sharedInstance.immediateMemoryScore += num
            }
        }
    }
    
    // function that is called when next button is pressed
    func leaveScene() {
        calculateScore()
        if (GlobalVariables.sharedInstance.delayedRecallFlag == 1 && GlobalVariables.sharedInstance.testStateFlag == 1) {
            performSegueWithIdentifier("wordtoScore", sender:self)
        }
        else if (wordMemoryTestTrialIndex >= 3) {
            performSegueWithIdentifier("wordToDigit", sender:self)
        }
        else {
            resetScene()
        }
    }
    
    // resets the scene variables && assigns new text to labels && increments the test trial index
    func resetScene() {
        wordMemoryTestTrialIndex++
        pressed = [0,0,0,0,0]
        
        // set header label text
        header.text = (GlobalVariables.sharedInstance.testStateFlag == 1 && GlobalVariables.sharedInstance.delayedRecallFlag == 1) ? "Delayed Recall" : "Immediate Memory"
        
        // reset the color of all the buttons to white
        for buttonView in wordButtons {
            buttonView.backgroundColor = UIColor(hexString: buttonColor)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        wordButtons.removeAll(keepCapacity: false)
        wordMemoryTestTrialIndex = 0
        pressed = [0,0,0,0,0]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}