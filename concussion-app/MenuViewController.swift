//
//  MenuViewController.swift
//  concussion-app
//
//  Created by Michael Mathew on 2/28/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit

class MenuViewController: OpenEarsViewController {
    
    var initTestLabel: UIButton!
    var balanceTestLabel: UIButton!
    var scatTestLabel: UIButton!
    
    override func viewDidLoad() {
        loadOpenEars()
        
        // Set up Navigation Bar UI elements
        self.navigationItem.title = "Concussion Analysis"
        var nav = navigationController?.navigationBar
        nav?.barTintColor = UIColor(hexString: "#a85ac9")
        nav?.tintColor = UIColor.whiteColor()
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        // Set up text color for nav bar
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        nav?.titleTextAttributes = titleDict as [NSObject : AnyObject]
        
        self.view.backgroundColor = UIColor(hexString: "#F8F8F8")
        
        //center the button
        var xloc : CGFloat = screenWidth / 2.0
        var yloc : CGFloat = screenHeight / 2.0
        
        var buttonw : CGFloat = screenWidth
        var buttonh : CGFloat = screenHeight * 0.15
        
        // Set up Start Maddocks
        yloc = screenHeight*0.30
        
        initTestLabel = UIButton(frame: CGRectMake(0,yloc,buttonw,buttonh))
        initTestLabel.layer.cornerRadius = 5
        initTestLabel.layer.borderWidth = 1
        initTestLabel.layer.borderColor = UIColor.whiteColor().CGColor
        initTestLabel.layer.backgroundColor = UIColor(hexString: "#a85ac9")?.CGColor
        initTestLabel.setTitle("Start Maddocks", forState: UIControlState.Normal)
        initTestLabel.addTarget(self, action: "toMaddocks", forControlEvents: UIControlEvents.TouchUpInside)
        
        // Set up Balance Test Label
        yloc += 1.5*buttonh
        balanceTestLabel = UIButton(frame: CGRectMake(0,yloc,buttonw,buttonh))
        balanceTestLabel.layer.cornerRadius = 5
        balanceTestLabel.layer.borderWidth = 1
        balanceTestLabel.layer.borderColor = UIColor.whiteColor().CGColor
        balanceTestLabel.setTitle("Start Balance", forState: UIControlState.Normal)
        balanceTestLabel.layer.backgroundColor = UIColor(hexString: "#a85ac9")?.CGColor
        balanceTestLabel.addTarget(self, action: "toBalance", forControlEvents: UIControlEvents.TouchUpInside)
        
        // Set up Start SCAT test
        yloc += 1.5*buttonh
        scatTestLabel = UIButton(frame: CGRectMake(0,yloc,buttonw,buttonh))
        scatTestLabel.layer.cornerRadius = 5
        scatTestLabel.layer.borderWidth = 1
        scatTestLabel.layer.borderColor = UIColor.whiteColor().CGColor
        scatTestLabel.layer.backgroundColor = UIColor(hexString: "#a85ac9")?.CGColor
        scatTestLabel.setTitle("Start SCAT3", forState: UIControlState.Normal)
        scatTestLabel.addTarget(self, action: "toSCAT", forControlEvents: UIControlEvents.TouchUpInside)
        
        self.view.addSubview(scatTestLabel)
        self.view.addSubview(balanceTestLabel)
        self.view.addSubview(initTestLabel)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        self.openEarsEventsObserver.delegate = nil
        self.openEarsEventsObserver = nil
    }
    
    func toSCAT() {
        GlobalVariables.sharedInstance.testStateFlag = 1
        performSegueWithIdentifier("startSCAT",sender: self)
    }
    func toBalance() {
        GlobalVariables.sharedInstance.indepBalanceFlag = 1
        GlobalVariables.sharedInstance.testStateFlag = 2
        performSegueWithIdentifier("startBalance",sender: self)
    }
    func toMaddocks() {
        performSegueWithIdentifier("startMaddocks",sender: self)
    }
}
