//
//  digitMemory_title.swift
//  concussion-app
//
//  Created by Alex Rumbaugh on 3/1/15.
//  Copyright (c) 2015 Genesis. All rights reserved.
//

import UIKit

class digitMemoryTestViewController: OpenEarsViewController {
    
    
    
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var delButton: UIButton!
    
    @IBOutlet var instructionsLabel: UILabel!
    
    @IBOutlet var digits: [UILabel] = []
    
    @IBOutlet var keypad: [UIButton] = []
    
    var keypadStrings: [String] = ["1","2","3","4","5","6",
        "7","8","9","0","DEL"];
    
    var buttonStrings: [String] = ["", "", "", "", ""]
    
    //the buttons the user has selected
    var buttonsPressed: [Int] = [-1,-1,-1,-1,-1]
    
    @IBAction func buttonPress(sender : UIButton) {
        
        var digittext = sender.titleLabel!.text!
        placeDigit(digittext)
    }
    
    @IBAction func delButtonPress(sender : UIButton) {
        removeDigit()
    }
    
    @IBAction func about(sender: UIButton) {
        digitMemoryTestInst.currentTrial++
        if (digitMemoryTestInst.currentTrial < 3)
        {
            performSegueWithIdentifier("nextIteration", sender: sender)
        }
        else
        {
            performSegueWithIdentifier("nextScene", sender: sender)
        }
    }
    
    
    @IBAction func nextScene(sender : UIButton) {
        
        
    }
    
    func placeDigit(digittext: NSString)
    {
        let digitsPressed = digitMemoryTestInst.digitsPressed
        let totalDigits = digitMemoryTestInst.digitsPerTrial[digitMemoryTestInst.currentTrial]

        
        if(digitsPressed < totalDigits)
        {
            buttonStrings[digitsPressed] = digittext as String
            digits[digitsPressed].text = digittext as String
            (digitMemoryTestInst.digitsPressed)++
        }
    }
    
    func removeDigit()
    {
        let digitsPressed = digitMemoryTestInst.digitsPressed
        if(digitsPressed > 0)
        {
            buttonStrings[digitsPressed - 1] = ""
            digits[digitsPressed-1].text = ""
            (digitMemoryTestInst.digitsPressed)--
        }
    }
    
    func calculateScore()
    {
        // maintain index for digits we generated and and use i to check the digits we choose starting from reverse
        var index = 0
        if(digitMemoryTestInst.digitsPressed == 0) {
            return
        }
        for i in 1...digitMemoryTestInst.digitsPressed {
            if( String(digitMemoryTestInst.chosenDigits[index]) != buttonStrings[digitMemoryTestInst.digitsPressed - i]) {
                return
            }
            index++
        }
        GlobalVariables.sharedInstance.concentrationScore++
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.enabled = true

        
        //setup navigation bar title
        self.navigationItem.title = "Cognitive Assessment"
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        self.view.backgroundColor = UIColor(hexString:bgColor)
        
        
        //border dimensions and measurements
        var bounds: CGRect = UIScreen.mainScreen().bounds
        var width:CGFloat = bounds.size.width
        var height:CGFloat = bounds.size.height
        let ypad_top : CGFloat = height * 0.15
        let ypad_bot : CGFloat = height * 0.35
        let xpad_left : CGFloat = width * 0.10
        let xpad_right : CGFloat = width * 0.10
        
       
        let xpad_display : CGFloat = width * 0.05
        
        
        //used to position the digits on the screen
        var numDigits:CGFloat = CGFloat(digitMemoryTestInst.digitsPerTrial[digitMemoryTestInst.currentTrial])
        let celldigx : CGFloat = (width - (2 * xpad_display) )/numDigits
        let celldigy : CGFloat = (height - ypad_top - ypad_bot)
        
        //used to position the keypad on the screen
        let cellkeyx : CGFloat = (width - xpad_left - xpad_right)/3.0
        let cellkeyy : CGFloat = (0.80)*ypad_bot / 4.0
        
        //width & height of the buttons
        let keypadw :CGFloat = cellkeyx - 3
        let keypadh :CGFloat = cellkeyy - 3
        
        let digitw : CGFloat = 48
        let digith : CGFloat = 1.6*48
        
        //font size variables
        var digitFontSize : CGFloat = 32
        var keypadFontSize: CGFloat = 18
        var titleFontSize : CGFloat = 24
        
        var header = UILabel(frame: CGRectMake(0, 0, 280, 45))
        header.center = CGPointMake((screenWidth/2), (screenHeight/6))
        header.textAlignment = NSTextAlignment.Center
        header.font = UIFont(name: header.font.fontName, size: 36)
        header.layer.cornerRadius = 5
        header.layer.masksToBounds = true
        header.text = "Concentration"
        self.view.addSubview(header)
        
        //insert first question
        var q2 = UILabel(frame: CGRectMake(0, 0, 280, 45))
        q2.center = CGPointMake((screenWidth/2), (screenHeight/4))
        q2.textAlignment = NSTextAlignment.Center
        q2.font = UIFont(name: q2.font.fontName, size: 20)
        q2.layer.cornerRadius = 5
        q2.layer.masksToBounds = true
        q2.text = "Reverse the Sequence"
        self.view.addSubview(q2)
        
        
        //generate the digits for this trial
        var i = 0
        var row :CGFloat = 0
        var col :CGFloat = 0
        for key in keypad
        {
            var xpos: CGFloat = xpad_left + cellkeyx*col + cellkeyx/2.0
            var ypos: CGFloat = height - ypad_bot + cellkeyy*row + cellkeyy/2.0
            key.frame = CGRectMake(xpos,ypos,keypadw,keypadh)
            key.center = CGPoint(x: xpos, y: ypos)
            key.setTitle(keypadStrings[i], forState: UIControlState.Normal)
            key.titleLabel!.font = UIFont(name: key.titleLabel!.font.fontName, size: keypadFontSize)
            key.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            key.layer.borderColor = UIColor.blackColor().CGColor
            key.layer.borderWidth = 1.5
            key.backgroundColor = UIColor(hexString: buttonColor)
            col++
            if(col == 3)
            {
                row++
                col=0
            }
            i++
        }


        
        //set the labels with digits
        i = 0
        for digitLabel in digits
        {
            digitLabel.backgroundColor = UIColor.whiteColor()
            if ( i >= Int(numDigits))
            {
                /// don't display it!
                digitLabel.frame = CGRectMake(0,0,0, 0)
            }
            else
            {
                //display the digit box
                //digitLabel.text = digitMemoryTestInst.chosenDigits[i]
                var xpos: CGFloat = xpad_display + celldigx*CGFloat(i) + celldigx/2.0
                var ypos: CGFloat = ypad_top + celldigy/2.0
                digitLabel.frame = CGRectMake(xpos,ypos,digitw, digith)
                digitLabel.center = CGPoint(x: xpos,y: ypos)

                digitLabel.layer.borderColor = UIColor.blackColor().CGColor
                digitLabel.layer.borderWidth = 1.0
                digitLabel.backgroundColor = UIColor(hexString: buttonColor)
                digitLabel.font = UIFont(name: digitLabel.font.fontName, size: digitFontSize)
            }
            i++
            
        }
        //reset the digits pressed
        digitMemoryTestInst.digitsPressed = 0
        //reset correct digits
        digitMemoryTestInst.correctDigits = 0
        
        
        nextButton.enabled = false
        // Text-To-Speech command for test
        voiceCommand("Please specify the digits from the previous screen in reverse order")
        
        // voice recognition
        addWords()
        changeLanguageModel()
    }
    
    // event is fired AT THE END OF THE VOICE DICTATION
    override func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didFinishSpeechUtterance utterance: AVSpeechUtterance!) {
        nextButton.enabled = true
        resumeRecognition()
    }
    
    
    func printCorrect(){
        print("Correct digits: \(digitMemoryTestInst.correctDigits)")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
    *  BEGIN VOICE RECOGNITION METHODS
    */
    
    // method to build language model for scene
    override func addWords() {
        words.append("0")
        words.append("1")
        words.append("2")
        words.append("3")
        words.append("4")
        words.append("5")
        words.append("6")
        words.append("7")
        words.append("8")
        words.append("9")
        words.append("NEXT")
    }
    
    // voice recognition receiver method
    override func pocketsphinxDidReceiveHypothesis(hypothesis: String, recognitionScore: String, utteranceID: String) {
        let numtext = hypothesis.componentsSeparatedByString(" ")[0]
        if(hypothesis == "NEXT") {
            about(nextButton)
        }
        else {
            placeDigit(numtext as NSString)
        }
    }
    
    // on scene change perform clean up actions
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        calculateScore()
        suspendRecognition()
        removeWords()
        removeButtons()
        self.openEarsEventsObserver.delegate = nil
        self.openEarsEventsObserver = nil
    }
    
    /**
    *  END VOICE RECOGNITION METHODS
    */
    
    
    
}
