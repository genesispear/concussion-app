//
//  OpenEarsViewController.swift
//  LOLTest
//
//  Created by Michael Mathew on 4/11/15.
//  Copyright (c) 2015 Michael Mathew. All rights reserved.
//

import Foundation
import AVFoundation

var lmPath: String!
var dicPath: String!
var words: Array<String> = []
var currentWord: String!
var openEarsStartedFlag = false


// dictionary to get reference to UIButton for voice recognition
var buttonArray =  [String: UIButton]()

class OpenEarsViewController: UIViewController, OEEventsObserverDelegate, AVSpeechSynthesizerDelegate {
    
    var openEarsEventsObserver: OEEventsObserver!
    let synth = AVSpeechSynthesizer()
    
    
    //OpenEars methods begin
    
    func loadOpenEars() {
        self.openEarsEventsObserver = OEEventsObserver()
        self.openEarsEventsObserver.delegate = self
        //3.8
        OEPocketsphinxController.sharedInstance().vadThreshold = 3.5
        OEPocketsphinxController.sharedInstance().secondsOfSilenceToDetect = 0.5
        OEPocketsphinxController.sharedInstance().removingSilence = true
        OEPocketsphinxController.sharedInstance().removingNoise = true
        
        var lmGenerator: OELanguageModelGenerator = OELanguageModelGenerator()
        var name    = "LanguageModelFileStarSaver"
        lmGenerator.generateLanguageModelFromArray(words, withFilesNamed: name, forAcousticModelAtPath: OEAcousticModel.pathToModel("AcousticModelEnglish"))
        
        lmPath = lmGenerator.pathToSuccessfullyGeneratedLanguageModelWithRequestedName(name)
        dicPath = lmGenerator.pathToSuccessfullyGeneratedDictionaryWithRequestedName(name)
    }
    
    func changeLanguageModel() {
        OEPocketsphinxController.sharedInstance().setActive(true, error: nil)
        self.openEarsEventsObserver = OEEventsObserver()
        self.openEarsEventsObserver.delegate = self
        
        var lmGenerator: OELanguageModelGenerator = OELanguageModelGenerator()
        var name    = "LanguageModelFileStarSaver"
        lmGenerator.generateLanguageModelFromArray(words, withFilesNamed: name, forAcousticModelAtPath: OEAcousticModel.pathToModel("AcousticModelEnglish"))
        
        lmPath = lmGenerator.pathToSuccessfullyGeneratedLanguageModelWithRequestedName(name)
        dicPath = lmGenerator.pathToSuccessfullyGeneratedDictionaryWithRequestedName(name)
        
        OEPocketsphinxController.sharedInstance().changeLanguageModelToFile(lmPath, withDictionary: dicPath)
    }
    
    func pocketsphinxDidReceiveHypothesis(hypothesis: String, recognitionScore: String, utteranceID: String) {
        //method for when open ears ACTUALLY RECEIVES INPUT
    }
    
    func pocketsphinxDidStartListening() {
        // method for when open ears starts listening
    }
    
    func pocketsphinxDidStopListening() {
        //method for when open ears stops listening
    }
    
    func pocketsphinxDidDetectSpeech() {
        // println("detected")
    }
    
    func pocketsphinxDidDetectFinishedSpeech() {
        // println("Finished")
    }
    
    func pocketsphinxDidSuspendRecognition() {
        // println("suspended")
    }
    
    func pocketsphinxDidResumeRecognition() {
        //println("Resumed")
    }
    
    func suspendRecognition() {
        OEPocketsphinxController.sharedInstance().suspendRecognition()
    }
    
    func resumeRecognition() {
        if (shouldStartListening()) {
            startListening()
        }
        else {
            OEPocketsphinxController.sharedInstance().resumeRecognition()
        }
    }
    
    func startListening() {
        OEPocketsphinxController.sharedInstance().setActive(true, error: nil)
        OEPocketsphinxController.sharedInstance().startListeningWithLanguageModelAtPath(lmPath, dictionaryAtPath: dicPath, acousticModelAtPath: OEAcousticModel.pathToModel("AcousticModelEnglish"), languageModelIsJSGF: false)
        openEarsStartedFlag = true
    }
    
    func stopListening() {
        OEPocketsphinxController.sharedInstance().stopListening()
    }
    
    func shouldStartListening() -> Bool {
        return !openEarsStartedFlag
    }
    
    func addWords() {
        //function to override to add words
    }
    
    func removeWords() {
        words.removeAll(keepCapacity: false)
    }
    
    func removeButtons() {
        buttonArray.removeAll(keepCapacity: false)
    }
    
    //OpenEars methods end
    
    // method for Text-To-Speech command
    func voiceCommand(commandString: String) {
        var myUtterance = AVSpeechUtterance(string: commandString)
        myUtterance.rate = 0.05
        synth.delegate = self
        synth.speakUtterance(myUtterance)
    }
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didFinishSpeechUtterance utterance: AVSpeechUtterance!) {
        // method for event triggering at the end of the Voice Dictation
    }
}